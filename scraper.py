#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time         #sleep, parse timestamp
import subprocess   #call external processes
import sys          #sys.stdout
import pprint       #pretty printing of primitive objects
import MySQLdb as mdb   # python mysql/mariadb bindings
import re           #regular expressions
import threading    #threads
from threading import Lock
import logging      #
import logging.handlers
from collections import deque   # deque
import os           # clear screen
import datetime
import signal       # Ctrl+C
import random       # random waiting time after getting an error from mysql
import yaml         # config file
import argparse     # command line arguments

ap = argparse.ArgumentParser()
ap.add_argument("-c", "--config", help="specify config file name", default="/etc/alienscraper.conf")
ap.add_argument("-l", "--log", help="specify log file name (unspecified=stdout)", default="-")
args = ap.parse_args()

with open(args.config, "r") as f:
    config = yaml.load(f, Loader=yaml.SafeLoader)

# AliEn configuration
alienpy_executable = config['alien']['alien.py_executable']

# Database configuration
db_host = config['database']['host']
db_user = config['database']['user']
db_pass = config['database']['pass']
db_db   = config['database']['db']

# Scraper configuration
log_file = args.log
log_level = config['scraper']['log_level']
log_format = config['scraper']['log_format']
#alien_site gets read from the database using get_site()
num_threads_top = config['scraper']['num_threads_top']
num_threads_ps = config['scraper']['num_threads_ps']
# num_threads_masterjob = config['scraper']['num_threads_masterjob']
num_threads_histogram_updater_day = config['scraper']['num_threads_histogram_updater_day']
num_threads_histogram_updater_hour = config['scraper']['num_threads_histogram_updater_hour']
num_threads_histogram_updater_wns_day = config['scraper']['num_threads_histogram_updater_wns_day']
wait_db_error_min_seconds = config['scraper']['wait_db_error_min_seconds']
wait_db_error_max_seconds = config['scraper']['wait_db_error_max_seconds']
wait_between_top_scrapes_seconds = config['scraper']['wait_between_top_scrapes_seconds']


CL_OPTION = "\033[94m"
CL_OK = "\033[92m"
CL_WARNING = "\033[93m"
CL_ERROR = "\033[91m"
CL_END = "\033[0m"


# Global stop flag.
# Read by all scraper threads
f_stop = False

# http://stackoverflow.com/questions/3791398/how-to-stop-python-from-propagating-signals-to-subprocesses
def preexec(): # Don't forward signals.
    os.setpgrp()

def helper_print_stdout_stderr(in_stdout_list, in_stderr_value, in_max_num_lines_stdout, in_max_num_lines_stderr):
    """
    in: list in_stdout_list, string in_stderr_value, int in_max_num_lines
    out: True
    """

    logging.debug("stdout (max 5 lines):")
    if len(in_stdout_list) < 5:
        pp = pprint.PrettyPrinter(depth=2)
        pp.pprint(in_stdout_list)
    else:
        pp = pprint.PrettyPrinter(depth=2)
        pp.pprint(in_stdout_list[0:5])

    logging.debug("stderr:")
    pp = pprint.PrettyPrinter(depth=2)
    pp.pprint(in_stderr_value)

# https://stackoverflow.com/questions/1322464/python-time-format-check
def isTimeFormat(input):
    try:
        time.strptime(input, '%H:%M:%S')
        return True
    except ValueError:
        return False

def database_connection():
    return mdb.connect(db_host, db_user, db_pass, db_db)

class Work_Item_Queue:
    """Provides a class containing a deque for job_ids.
    Methods:
    get(consumer_id): consumer consumer_id gets a new job_id from the left end of the deque
    put(job_ids): append the iterable job_ids to the right end of the deque
    """
    def __init__(self, name):
        self.name = name
        self.d = deque()    # The deque itself
        self.c = dict()     # This dictionary will remember the job_id last taken from a given consumer, for all consumers seen
        self.x = None       # Item last taken
        self.m = Lock()     # mutex
    def get(self, consumer_id):
        if consumer_id == None:
            return None
        else:
            self.m.acquire()
            self.x = None
            if len(self.d)>0:                   # deque not empty
                self.x = self.d.popleft()       # take the next item
                self.c[consumer_id] = self.x    # remember that the consumer consumer_id last took item x
            #print("returning %s" % self.x)
            self.m.release()
            return self.x
    def put(self, extension):
        self.d.extend(extension)
    def count(self):
        return len(self.d)

class Work_Item_Queue_Filler(threading.Thread):
    """Thread that refills the given Work_Item_Queue *work_item_queue* using the query *query*
    When the number of available items in work_item_queue drops below *low*, it will request *new* items.
    The query will get *new* items in total (as opposed to: new items which we don't have already).
    For that reason, *new* should be significantly bigger than *low*.
    The queue fill level will be checked every *sleep_seconds* seconds."""
    def __init__(self, in_name, in_work_item_queue, in_query, in_low, in_new, in_sleep_seconds):
        threading.Thread.__init__(self)
        self.daemon = True
        self.name = in_name
        self.work_item_queue = in_work_item_queue
        self.query = in_query
        self.low = in_low
        self.new = in_new
        self.sleep_seconds = in_sleep_seconds
        self.current_queue_items = []
        self.last_activity = datetime.datetime.now()
        self.last_message = "Thread Work_Item_Queue_Filler('%s') created" % self.name
        self.state = "started"
        self.next_state = None
        self.terminate = False        # set this to True from the outside to tell the thread to terminate
        logging.info("Work_Item_Queue_Filler: object %s created" % self.name)
    def run(self):
        while not self.terminate:
            self.last_activity = datetime.datetime.now()
            logging.debug("state=%s" % self.state)
            if self.state=="started":
                self.next_state = "check_stop_flag"
            elif self.state=="check_stop_flag":
                if f_stop:
                    self.next_state = "sleeping"
                else:
                    self.next_state = "check_queue_fill_level"
            elif self.state=="check_queue_fill_level":
                self.current_queue_items = set(self.work_item_queue.d)                # queue
                self.current_worked_on_items = set(dict.values(self.work_item_queue.c))     # items that are currently being worked on by the consumers
                self.exclusions = self.current_queue_items.union(self.current_worked_on_items)
                logging.debug("current_queue_items: %s" % str(self.current_queue_items))
                logging.debug("current_worked_on_items: %s" % str(self.current_worked_on_items))
                logging.debug("total exclusions: %s" % str(self.exclusions))
                logging.debug("currently %d items in queue or in progress" % len(self.exclusions))
                if len(self.exclusions)<self.low:
                    logging.debug("that is less than %d, so we request new items from the DB." % self.low)
                    self.next_state = "ask_db_for_work_items"
                else:
                    self.next_state = "sleeping"
            elif self.state=="ask_db_for_work_items":
                self.query = self.query.replace("__NEW__", str(self.new))
                logging.debug("query=%s" % self.query)
                dbc = database_connection()
                cur = dbc.cursor()
                cur.execute(self.query)
                self.result = cur.fetchall()
                cur.close()
                dbc.close()
                self.new_items = set(set(self.result) - self.exclusions)
                logging.debug("new items for queue: %s" % str(self.new_items))
                self.next_state = "fill_queue"
            elif self.state=="fill_queue":
                self.work_item_queue.put(self.new_items)
                logging.debug("filled queue. contents now: %s" % str(self.work_item_queue.d))
                self.next_state = "sleeping"
            elif self.state=="sleeping":
                time.sleep(self.sleep_seconds)
                self.next_state = "check_stop_flag"

            self.state = self.next_state

def scraper_ps(in_job_id, in_arguments):
    # Get master_job_id, worker_node, and state transitions of the specified job_id
    # and put them into our database.
    # Because we get the state transitions "for free" when running ps trace <job_id>,
    # there is no additional load on the alien servers due to that.
    # master_job_id (if exists) is stored in column "master_job_id", otherwise "-1".

    in_job_id = in_job_id[0] # Input in_job_id is a tuple with only one item

    state = "st_get_database_connection"
    next_state = None

    while state != "st_terminate":
        if state == "st_get_database_connection":
            dbc = database_connection()
            if dbc == None:
                next_state = "st_e_could_not_get_database_connection"
            else:
                next_state = "st_get_old_values_from_db"

        elif state == "st_e_could_not_get_database_connection":
            logging.error("%sWARNING%s: Could not get MySQL connection. Full error message: %s" % (CL_WARNING, CL_END, str(e)))
            wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)
            next_state = "st_get_database_connection"

        elif state == "st_get_old_values_from_db":
            # get c_category and scrape_status BEFORE adding new transitions
            query_get_infos_t_jobs = "SELECT c_category, scrape_status FROM t_jobs WHERE job_id=%s"
            cur = dbc.cursor()
            try:
                cur.execute(query_get_infos_t_jobs, [in_job_id])
                next_state = "st_get_old_values_from_db_2"
            except (mdb.Error, mdb.Warning) as e:
                logging.warning("%sWARNING%s: MySQL error occurred while trying to get c_category and scrape_status for JobID %s. Trying again. Full error message: %s" % (CL_WARNING, CL_END, in_job_id, str(e)))
                next_state = "st_e_could_not_get_old_values_from_db"

        elif state == "st_get_old_values_from_db_2":
            result = cur.fetchall()
            result_c_category_before = result[0][0]
            result_scrape_status_before = result[0][1]
            next_state = "st_call_alien_py"

        elif state == "st_e_could_not_get_old_values_from_db":
            cur.close()
            wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)
            next_state = "st_get_old_values_from_db"

        elif state == "st_call_alien_py":
            alien_command = alienpy_executable + ' ps -trace %s'
            logging.debug("Running AliEn command: " + alien_command % in_job_id)
            p = subprocess.Popen(alien_command % in_job_id, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn = preexec)
            stdout_value,stderr_value = p.communicate()
            stdout_list = str(stdout_value).split("\\n")
            logging.debug("AliEn command returned " + str(len(stdout_list)) + " lines.")
            next_state = "st_extract_data"

        elif state == "st_extract_data":
            # Order does matter, first match wins.
            search_regexes_transition = [
                "Job state transition.*?to (\w*)",
                "Killing the job \((.*)\)",
                "The job has been resubmited \(back to (\w*)\)" # resubmited [sic]
                # "Job has been killed"
            ]
            search_regex_jobagent_id = "The Job has been taken by Jobagent (\w*)," #does it take underscores? 1234_12345_12345
            search_regex_master_job = [
                "Master Job is (\d*)",
                "MasterJobId = \"(\d*)\""
            ]
            search_regex_site_name = "Job ASSIGNED to: ([A-Za-z0-9\-_]+::[A-Za-z0-9\-_]+::[A-Za-z0-9\-_]+)"
            search_regex_worker_node = "Running.*on ([A-Za-z0-9.:-]+)"
            search_regex_time = "^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})"
            search_regex_slurm_job_id = "BatchId SLURM_JOB_ID: (\d*)"

            transitions_to_be_written = []

            # The MasterJobID is assumed to be constant.
            # Once we found it for a given job_id somewhere in the ps trace output, we keep it.
            d_job_master_job_id = -1

            d_job_site_name = None
            d_job_worker_node = None
            d_job_new_state = None
            d_jobagent_id = None
            d_slurm_job_id = None

            d_alien_line_number = 0
            for x in stdout_list:
                d_alien_line_number += 1

                b_found_transition = False

                # We keep the old states of d_job_site_name and d_job_worker_node
                # If we get new ones, we clear these flags.
                # The state of these flags are stored in the DB
                d_job_site_name_speculative = True
                d_job_worker_node_speculative = True

                # master_job_id
                if d_job_master_job_id == -1:
                    for y in search_regex_master_job:
                        p = re.compile(y)
                        found = p.search(x)
                        if found:
                            d_job_master_job_id = found.group(1)
                            logging.debug("Found MasterJobID %s in the following line: %s" % (d_job_master_job_id, x))
                            break   #don't try further regexes, we already found a match

                # site_name
                p = re.compile(search_regex_site_name)
                found = p.search(x)
                if found:
                    d_job_site_name = found.group(1)
                    d_job_site_name_speculative = False

                if found:
                    logging.debug("Found site_name = %s (speculative=%r)" % (d_job_site_name, d_job_site_name_speculative))
                else:
                    logging.debug("site_name not found")

                # worker_node
                p = re.compile(search_regex_worker_node)
                found = p.search(x)
                if found:
                    d_job_worker_node = found.group(1)
                    d_job_worker_node_speculative = False

                # jobagent_id
                if d_jobagent_id == None:
                    p = re.compile(search_regex_jobagent_id)
                    found = p.search(x)
                    if found:
                        d_jobagent_id = found.group(1)

                # slurm_job_id
                if d_slurm_job_id == None:
                    p = re.compile(search_regex_slurm_job_id)
                    found = p.search(x)
                    if found:
                        d_slurm_job_id = found.group(1)

                # any transition
                for y in search_regexes_transition:
                    # logging.debug("\n")
                    # logging.debug("Testing search_regex '%s'" % y)
                    # logging.debug("on string '%s'" % x)

                    p = re.compile(y)
                    found = p.search(x)
                    if found:
                        if "Killing" in y:
                            d_job_new_state = "Killed: "+found.group(1)
                        else:
                            d_job_new_state = found.group(1)

                        p2 = re.compile(search_regex_time)
                        found2 = p2.search(x)
                        if found2:
                            b_found_transition = True
                            transitions_to_be_written.append([in_job_id, found2.group(1), d_alien_line_number, d_job_site_name, d_job_site_name_speculative, d_job_new_state, d_job_worker_node, d_job_worker_node_speculative, d_jobagent_id, d_slurm_job_id])
                            break   #don't try further regexes, we already found a match
                        else:
                            logging.warning("%sWARNING%s Couldn't find a valid time using regex '%s' in line '%s'" % (CL_WARNING, CL_END, search_regex_time, x))
                            pass
                    # else:
                    #     logging.debug("no match found")
            next_state = "st_insert_transitions"

        elif state == "st_insert_transitions":
            query_insert_transition = "INSERT IGNORE INTO t_job_transitions (job_id, t, alien_line_number, site, speculative_site, new_state, worker_node, speculative_worker_node, jobagent_id, slurm_job_id) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            ntr = 0
            for x in transitions_to_be_written:
                ntr += 1
                b = False
                while not b:
                    try:
                        cur.execute(query_insert_transition, (x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9]))
                        b = True
                        logging.debug("Transition (%d/%d): job_id=%s, t=%s, l=%s, site=%s%s%s, sp'_site=%r, new_state=%s%s%s, worker_node=%s, sp'_worker_node=%r, jobagent_id=%s, slurm_job_id=%s" % (ntr, len(transitions_to_be_written), x[0], x[1], x[2], CL_OPTION, x[3], CL_END, x[4], CL_OPTION, x[5], CL_END, x[6], x[7], x[8], x[9]))
                        if ntr%100==0 or ntr==len(transitions_to_be_written): # Log a message only show every 100 lines, or when having processed the last one
                            logging.debug("Committing transaction after %d of %d transitions for JobID %s" % (ntr, len(transitions_to_be_written), in_job_id))
                            dbc.commit()
                    except (mdb.Error, mdb.Warning) as e:
                        logging.error("%sERROR%s: MySQL error occurred while trying to insert transition. Trying again. Full error message: %s" % (CL_WARNING, CL_END, str(e)))
                        wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)

            next_state = "st_update_t_jobs"

        elif state == "st_update_t_jobs":
            query_update_t_jobs = "UPDATE t_jobs SET master_job_id=%s, time_ps=NOW() WHERE job_id=%s"
            try:
                cur.execute(query_update_t_jobs, (d_job_master_job_id, in_job_id))
                dbc.commit()
                next_state = "st_call_p_update_job_details"
            except (mdb.Error, mdb.Warning) as e:
                logging.error("%sERROR%s: MySQL error occurred while trying to update t_jobs where job_id=%s. Trying again. Full error message: %s" % (CL_WARNING, CL_END, in_job_id, str(e)))
                next_state = "st_e_update_t_jobs_failed"

        elif state == "st_e_update_t_jobs_failed":
            next_state = "st_update_t_jobs"
            wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)

        elif state == "st_call_p_update_job_details":
            query_update_t_jobs = "CALL p_update_job_details(%s)"
            try:
                cur.execute(query_update_t_jobs, [in_job_id])
                next_state = "st_get_new_values_from_db"
            except (mdb.Error, mdb.Warning) as e:
                logging.error("%sERROR%s: MySQL error occurred while trying to call p_update_job_details(%s). Trying again. Full error message: %s" % (CL_WARNING, CL_END, in_job_id, str(e)))
                next_state = "st_e_call_p_update_job_details_failed"

        elif state == "st_e_call_p_update_job_details_failed":
            next_state = "st_call_p_update_job_details"
            wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)

        elif state == "st_get_new_values_from_db":
            # get c_category and scrape_status AFTER adding new transitions
            query_get_infos_t_jobs = "SELECT c_category, scrape_status, user FROM t_jobs WHERE job_id=%s"
            try:
                cur.execute(query_get_infos_t_jobs, [in_job_id])
                next_state = "st_get_new_values_from_db_2"
            except (mdb.Error, mdb.Warning) as e:
                logging.error("%sERROR%s: MySQL error occurred while trying to get c_category and scrape_status for JobID %s. Trying again. Full error message: %s" % (CL_WARNING, CL_END, in_job_id, str(e)))
                next_state = "st_e_could_not_get_new_values_from_db"

        elif state == "st_e_could_not_get_new_values_from_db":
            next_state = "st_get_new_values_from_db"
            wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)

        elif state == "st_get_new_values_from_db_2":
            result = cur.fetchall()
            result_c_category_after = result[0][0]
            result_scrape_status_after = result[0][1]
            result_user = result[0][2]
            logging.info("INFO: JobID %s: user=%s category=%s->%s%s%s scrape_status=%s->%s" % (in_job_id, result_user, result_c_category_before, CL_OK, result_c_category_after, CL_END, result_scrape_status_before, result_scrape_status_after))
            cur.close()
            logging.debug("Committing transaction and closing database connection")
            dbc.commit()
            dbc.close()
            next_state = "st_terminate"

        state = next_state

    return [ True, [ d_job_master_job_id, d_job_worker_node, stdout_value, transitions_to_be_written ]]

def scraper_top(in_arguments):
    in_site = in_arguments["site"]
    global f_stop

    alien_command = alienpy_executable + ' ps -L -a -s %s'
    logging.info("Running AliEn command: " + alien_command % in_site)

    p = subprocess.Popen(alien_command % in_site, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn = preexec)
    # pr = None
    # while pr == None and not f_stop:
    #     time.sleep(1)
    #     pr = p.poll()
    #     #logging.debug("top has not returned yet. pr = %s" % str(pr))
    # if f_stop:
    #     if pr == None:
    #         logging.debug("top did not return yet and the stop flag is set, killing top")
    #         p.kill()
    #
    #     return [ False, None ]
    #
    # logging.debug("top has returned. pr = %s" % str(pr))
    stdout_value,stderr_value = p.communicate()
    stdout_list = str(stdout_value).split("\\n")
    logging.info("AliEn command returned %d lines" % len(stdout_list))

    logging.debug("Opening database connection")
    dbc = database_connection()

    #helper_print_stdout_stderr(stdout_list, stderr_value, 5, 5)

    search_regex_top = "^\s*(\w+)\s*([0-9]+)\s*( [0-9]*)\s*([A-Za-z]+::[A-Za-z]+::[0-9A-Za-z_]+)\s*(\S+)\s*([A-Z]+)\s*(\S+)\s*(\S+)"

    k = 0   #counts through the number of lines in stdout_list
    N = 0   #counts the number of actually retrieved rows
    N_0 = 0 #counts the number of actually retrieved jobs which have been in our database already
    N_1 = 0 #counts the number of actually retrieved jobs which have been newly inserted into our database
    N_2 = 0 #counts the number of actually retrieved jobs which have been updated in our database
    N_3 = 0 #counts the number of actually retrieved jobs which have been processed (either inserted, updated, or left as is) by our database
    for x in stdout_list:
        k+=1
        p = re.compile(search_regex_top)
        found = p.search(x)
        if found:
            d_user = found.group(1)
            d_job_id = found.group(2)
            # d_master_job_id = found.group(3) #ignored, gets inserted through scraper_ps
            # 4: site
            # 5: worker_node (ignored)
            # 6: status? (ignored)
            # 7: unknown (ignored)
            d_command = found.group(8)

            N+=1
            cur = dbc.cursor()

            b = False
            while not b:
                try:
                    cur.callproc("p_scraper_top", [ d_job_id, d_user, d_command, in_site, 0, 0 ])
                    b = True
                except (mdb.Error, mdb.Warning) as e:
                    logging.warning("%sWARNING%s: MySQL error occurred while trying to insert job. Trying again. Full error message: %s" % (CL_WARNING, CL_END, str(e)))
                    wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)

            proc_result = cur.fetchone()
            cur.close()

            if proc_result[0]==0:
                N_0+=1
            elif proc_result[0]==1:
                N_1+=1
            elif proc_result[0]==2:
                N_2+=1
            elif proc_result[0]==3:
                N_3+=1
            else:
                logging.error("p_scraper_top returned unexpected return code %s" % proc_result[0])

            #print(carriage_return+"[%d%% done. %d already in our database, %d newly inserted, %d updated, %d total.]" % ( (k*100./len(stdout_list)), N_0, N_1, N_2, N )),
            if N%100==0 or N==len(stdout_list): # Log a message only show every 100 lines, or when having processed the last one
                logging.debug("Processed %d of %d lines" % (N, len(stdout_list)))
                dbc.commit()

    logging.debug("Committing transaction and closing database connection")
    dbc.commit()
    dbc.close()
    logging.info("Done. %d unchanged, %d new, %d updated, %d processed, %d total." % ( N_0, N_1, N_2, N_3, N ))

# def scraper_masterjob(in_master_job_id, in_arguments):
#     """Gets number of jobs per site and status
#     and writes them to t_master_job_numbers.
#     Sets status and time_masterjob on t_master_jobs"""

#     search_regex_master_job_status = "is in status: (\w+)"
#     search_regex_master_job_subjobs_number = "Subjobs in (\w+) \(([A-Za-z0-9\-_]+::[A-Za-z0-9\-_]+::[A-Za-z0-9\-_]+)\): ([0-9]+)"

#     d_master_job_status = None

#     entries_to_be_written = []
#     master_job_gone = False

#     alien_command = alien_executable + ' login -exec "masterJob %s -printsite"'

#     logging.debug("Running AliEn command: " + alien_command % in_master_job_id)

#     p = subprocess.Popen(alien_command % in_master_job_id, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn = preexec)
#     stdout_value,stderr_value = p.communicate()
#     stdout_list = stdout_value.split("\n")

#     logging.debug("Command returned %d lines" % len(stdout_list))

#     helper_print_stdout_stderr(stdout_list, stderr_value, 5, 5)

#     p = re.compile(search_regex_master_job_status)
#     found = p.search(stdout_value)
#     if found:
#         master_job_gone = False
#         d_master_job_status = found.group(1)
#         logging.debug("master_job_id %s%s%s is in status %s%s%s" % (CL_OPTION, in_master_job_id, CL_END, CL_OPTION, d_master_job_status, CL_END))

#         for x in stdout_list:   # status/site/number triple
#             d_status = None
#             d_site_name = None
#             d_number = 0
#             p = re.compile(search_regex_master_job_subjobs_number)
#             found = p.search(x)
#             if found:
#                 d_status = found.group(1)
#                 d_site_name = found.group(2)
#                 d_number = found.group(3)

#                 entries_to_be_written.append([d_status, d_site_name, d_number])
#                 #logging.debug("d_status=%s%s%s, d_site_name=%s%s%s, d_number=%s%s%s" % (CL_OPTION, d_status, CL_END, CL_OPTION, d_site_name, CL_END, CL_OPTION, str(d_number), CL_END))

#     else:
#         master_job_gone = True
#         logging.warning("%sWARNING%s: Could not extract status for masterJob %s" % (CL_WARNING, CL_END, in_master_job_id))

#     dbc = database_connection()
#     cur = dbc.cursor()
#     if master_job_gone:
#         query = "INSERT INTO t_master_jobs (master_job_id, status, time_masterjob, master_job_gone) VALUES(%s, %s, NOW(), %s) ON DUPLICATE KEY UPDATE status=%s, time_masterjob=NOW(), master_job_gone=%s"
#         logging.debug("Setting time_masterjob and master_job_gone=1 for master_job_id=%s" % in_master_job_id)
#         cur.execute(query, [in_master_job_id, "gone", 1, "gone", 1])
#     else:
#         query = "DELETE FROM t_master_job_numbers WHERE master_job_id=%s"
#         logging.debug("Deleting any numbers for master_job_id=%s" % in_master_job_id)
#         cur.execute(query, [in_master_job_id])

#         query = "INSERT IGNORE INTO t_master_job_numbers (master_job_id, status, site, number) VALUES (%s, %s, %s, %s)"
#         for x in entries_to_be_written:
#             logging.debug("Writing entry: master_job_id=%s%s%s, status=%s%s%s, site=%s%s%s, number=%s%s%s" % (CL_OPTION, in_master_job_id, CL_END, CL_OPTION, x[0], CL_END, CL_OPTION, x[1], CL_END, CL_OPTION, x[2], CL_END))
#             cur.execute(query, (in_master_job_id, x[0], x[1], x[2]))

#         query = "INSERT INTO t_master_jobs (master_job_id, status, time_masterjob, master_job_gone) VALUES(%s, %s, NOW(), %s) ON DUPLICATE KEY UPDATE status=%s, time_masterjob=NOW(), master_job_gone=%s"
#         logging.debug("Setting status=%s, time_masterjob and master_job_gone=0 for master_job_id=%s" % (d_master_job_status, in_master_job_id))
#         cur.execute(query, [in_master_job_id, d_master_job_status, 0, d_master_job_status, 0])

#     logging.debug("Committing transaction and closing database connection")
#     cur.close()
#     dbc.commit()
#     dbc.close()

def histogram_updater(in_row, in_arguments):
    # Calls p_update_histogram(in_t, in_wn, in_interval_type)

    in_interval_type = in_arguments[0]
    in_t = in_row[0]
    in_wn = in_row[1]

    state = "st_get_database_connection"
    next_state = None

    while state != "st_terminate":
        if state == "st_get_database_connection":
            dbc = database_connection()
            if dbc == None:
                next_state = "st_e_could_not_get_database_connection"
            else:
                next_state = "st_call_p_update_histogram"

        elif state == "st_e_could_not_get_database_connection":
            logging.error("Error: %s" % state)
            wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)
            next_state = "st_get_database_connection"

        elif state == "st_call_p_update_histogram":
            query_update_histogram = "CALL p_update_histogram(%s, %s, %s)"
            cur = dbc.cursor()
            try:
                cur.execute(query_update_histogram, [in_t, in_wn, in_interval_type])
                next_state = "st_finalize"
            except (mdb.Error, mdb.Warning) as e:
                next_state = "st_e_p_update_histogram_failed"

        elif state == "st_e_p_update_histogram_failed":
            logging.error("%sERROR%s: MySQL error occurred while trying to call p_update_histogram(%s, %s, %s). Trying again. Full error message: %s" % (CL_WARNING, CL_END, in_t, in_wn, in_interval_type, str(e)))
            next_state = "st_call_p_update_histogram"
            wait_random_duration(wait_db_error_min_seconds,wait_db_error_max_seconds)

        elif state == "st_finalize":
            logging.info("INFO: Histogram created/updated: t=%s%s%s wn=%s%s%s interval_type=%s%s%s" % (CL_OPTION, in_t, CL_END, CL_OPTION, in_wn, CL_END, CL_OPTION, in_interval_type, CL_END))
            cur.close()
            dbc.commit()
            dbc.close()
            next_state = "st_terminate"

        state = next_state

    return [ True, [ in_t, in_interval_type ]]


class th_work_item_consumer(threading.Thread):
    """Consumes items from the Work_Item_Queue *work_item_queue*"""
    global f_stop
    def __init__(self, in_name, in_work_item_queue, in_sleep_seconds, in_worker_function, in_worker_function_arguments):
        threading.Thread.__init__(self)
        self.name = in_name
        self.work_item_queue = in_work_item_queue
        self.sleep_seconds = in_sleep_seconds   # sleep time in seconds after not being able to get a work item
        self.worker_function = in_worker_function
        self.worker_function_arguments = in_worker_function_arguments
        self.state = "started"
        self.next_state = None
        self.terminate = None        # set this to True from the outside to tell the thread to terminate
        logging.info("th_work_item_consumer: object %s created" % self.name)
    def run(self):
        while not self.terminate:
            self.last_activity = datetime.datetime.now()
            logging.debug("state=%s" % self.state)
            if self.state=="started":
                self.next_state = "check_stop_flag"
            elif self.state=="check_stop_flag":
                if f_stop:
                    self.next_state = "sleeping"
                else:
                    self.next_state = "try_to_get_item"
            elif self.state=="try_to_get_item":
                self.current_work_item = self.work_item_queue.get(self.name)
                if self.current_work_item==None:
                    logging.debug("couldn't get an item")
                    self.next_state = "sleeping"
                else:
                    logging.debug("got item: %s" % str(self.current_work_item))
                    self.next_state = "run_worker_function"
            elif self.state=="run_worker_function":
                logging.debug("running worker function on item %s" % str(self.current_work_item))
                self.worker_function(self.current_work_item, self.worker_function_arguments)
                self.next_state="check_stop_flag"
            elif self.state=="sleeping":
                time.sleep(self.sleep_seconds)
                self.next_state = "check_stop_flag"

            self.state = self.next_state
        logging.debug("Terminate flag set, terminating")

class th_interval(threading.Thread):
    global f_stop
    def __init__(self, in_name, in_sleep_seconds_short, in_sleep_seconds, in_worker_function, in_worker_function_arguments):
        threading.Thread.__init__(self)
        self.name = in_name
        self.sleep_seconds_short = in_sleep_seconds_short # "short" sleep time to catch that we should stop or terminate
        self.sleep_seconds = in_sleep_seconds   # sleep time in seconds after not being able to get a work item
        self.worker_function = in_worker_function
        self.worker_function_arguments = in_worker_function_arguments
        self.state = "started"
        self.next_state = None
        self.terminate = None        # set this to True from the outside to tell the thread to terminate
        self.time_out = None
        logging.info("th_interval: object %s created" % self.name)
    def run(self):
        while not self.terminate:
            self.last_activity = datetime.datetime.now()
            logging.debug("state=%s" % self.state)
            if self.state=="started":
                self.next_state = "perform_checks"
            elif self.state=="perform_checks":
                self.time_now = datetime.datetime.now()
                if f_stop:
                    self.next_state = "sleeping"
                else:
                    if self.time_out==None or self.time_out<self.time_now:
                        self.next_state = "run_worker_function"
                    else:
                        self.next_state = "sleeping"
            elif self.state=="run_worker_function":
                self.worker_function(self.worker_function_arguments)
                self.next_state="set_sleep_time"
            elif self.state=="set_sleep_time":
                self.time_now = datetime.datetime.now()
                self.time_sleep = datetime.timedelta(seconds=self.sleep_seconds)
                self.time_out = self.time_now + self.time_sleep
                logging.debug("sleeping until %s" % self.time_out)
                self.next_state = "perform_checks"
            elif self.state=="sleeping":
                time.sleep(self.sleep_seconds_short)
                self.next_state = "perform_checks"

            self.state = self.next_state
        logging.debug("Terminate flag set, terminating")

def wait_random_duration(sec_min, sec_max):
    r = random.randrange(sec_min, sec_max)
    logging.debug("Waiting %d seconds before trying again." % r)
    time.sleep(r)
    return True

def get_site():
    dbc = database_connection()
    cur = dbc.cursor()
    cur.execute("SELECT value FROM t_config WHERE keyname='site'")
    result = cur.fetchall()
    cur.close()
    dbc.close()
    return result[0]


# Logging
if log_file == "-":
    logging.basicConfig(stream=sys.stdout, level=log_level, format=log_format)
else:
    log_handler = logging.handlers.WatchedFileHandler(log_file)
    log_formatter = logging.Formatter(log_format)
    log_handler.setFormatter(log_formatter)
    log_logger = logging.getLogger()
    log_logger.addHandler(log_handler)
    log_logger.setLevel(log_level)

# read site to be scraped from database
alien_site = get_site()

threads = []

### scraper_top
for i in range(0, num_threads_top):
    scr_top = th_interval("scr_top_%d" % i, 1, wait_between_top_scrapes_seconds, scraper_top, {"site":alien_site})
    threads.append(scr_top)

### scraper_ps
if num_threads_ps > 0:
    logging.info("creating queue for scr_ps_*")
    q_ps = Work_Item_Queue("q_ps")

    logging.info("creating filler qf_ps for queue q_ps:")
    qf_ps = Work_Item_Queue_Filler("qf_ps",
                            q_ps,
                            "SELECT job_id FROM t_jobs WHERE time_next_scrape <= NOW() ORDER BY time_next_scrape ASC LIMIT __NEW__",
                            100,
                            200,
                            2)
    threads.append(qf_ps)

    for i in range(0, num_threads_ps):
        scr_ps_i = th_work_item_consumer("scr_ps_%d" % i, q_ps, 1, scraper_ps, [])
        threads.append(scr_ps_i)

### scraper_masterjob
# if num_threads_masterjob > 0:
#     logging.info("creating queue for scr_mj_*")
#     q_mj = Work_Item_Queue("q_mj")

#     qf_mj = Work_Item_Queue_Filler("qf_mj",
#                                 q_mj,
#                                 "SELECT master_job_id FROM v_queue_masterjobs WHERE f_needs_scraping_masterjob=1 ORDER BY master_job_id LIMIT __NEW__",
#                                 10,
#                                 100,
#                                 5)
#     threads.append(qf_mj)

#     for i in range(0, num_threads_masterjob):
#         scr_mj = th_work_item_consumer("scr_mj_%d" % i, q_mj, 1, scraper_masterjob, [])
#         threads.append(scr_mj)

### histogram updater
if num_threads_histogram_updater_day > 0:
    logging.info("creating queue for histogram_updater_day")
    q_hd = Work_Item_Queue("q_hd")

    logging.info("creating filler qf_hd for queue q_hd:")
    qf_hd = Work_Item_Queue_Filler("qf_hd",
                            q_hd,
                            "SELECT t, wn FROM v_queue_histograms_day ORDER BY t ASC LIMIT __NEW__",
                            400,
                            1000,
                            2)
    threads.append(qf_hd)

    for i in range(0, num_threads_histogram_updater_day):
        histogram_updater_day = th_work_item_consumer("histogram_updater_day_%d" % i, q_hd, 1, histogram_updater, ["day"])
        threads.append(histogram_updater_day)

if num_threads_histogram_updater_hour > 0:
    logging.info("creating queue for histogram_updater_hour")
    q_hh = Work_Item_Queue("q_hh")

    logging.info("creating filler qf_hh for queue q_hh:")
    qf_hh = Work_Item_Queue_Filler("qf_hh",
                            q_hh,
                            "SELECT t, wn FROM v_queue_histograms_hour ORDER BY t ASC LIMIT __NEW__",
                            400,
                            1000,
                            2)
    threads.append(qf_hh)

    for i in range(0, num_threads_histogram_updater_hour):
        histogram_updater_hour = th_work_item_consumer("histogram_updater_hour_%d" % i, q_hh, 1, histogram_updater, ["hour"])
        threads.append(histogram_updater_hour)

if num_threads_histogram_updater_wns_day > 0:
    logging.info("creating queue for histogram_updater_day_wns")
    q_hd_wns = Work_Item_Queue("q_hd_wns")

    logging.info("creating filler qf_hd_wns for queue q_hd_wns:")
    qf_hd_wns = Work_Item_Queue_Filler("qf_hd_wns",
                            q_hd_wns,
                            "SELECT t_start AS t, wn FROM t_c_category_data_wns WHERE interval_type='day' AND wn IS NOT NULL AND t_outdated IS NOT NULL ORDER BY t ASC LIMIT __NEW__",
                            400,
                            1000,
                            2)
    threads.append(qf_hd_wns)

    for i in range(0, num_threads_histogram_updater_wns_day):
        histogram_updater_day_wns = th_work_item_consumer("histogram_updater_day_wns_%d" % i, q_hd_wns, 1, histogram_updater, ["day"])
        threads.append(histogram_updater_day_wns)

# Start all threads
logging.info("starting threads")
for x in threads:
    logging.info("starting thread '%s'" % x.name)
    x.start()

# Wait for any signal
logging.info("waiting for signal")
try:
    signal.pause()
except KeyboardInterrupt:
    # Signal received, set stop flag
    logging.info("signal received")
    logging.info("setting stop flag")
    f_stop = True

    logging.info("setting the terminate flag for all threads")
    for x in threads:
        x.terminate = True

    # And join all threads
    logging.info("joining all threads")
    for x in threads:
        x.join()

logging.info("exiting")
