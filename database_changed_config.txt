[mysqld]
max_allowed_packet = 512M
datadir=/var/lib/mysql-ssd
innodb_data_home_dir = /var/lib/mysql-ssd
innodb_data_file_path = ibdata1:10M:autoextend
innodb_log_group_home_dir = /var/lib/mysql-ssd
innodb_buffer_pool_size = 6G
innodb_log_file_size = 2G
innodb_log_buffer_size = 2G
expire_log_days = 7

[mysqldump]
max_allowed_packet = 512M
