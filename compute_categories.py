#!/usr/bin/python2
# -*- coding: utf-8 -*-

import time         #sleep, parse timestamp
import subprocess   #call external processes
import sys          #command line arguments
import pprint       #pretty printing of primitive objects
import MySQLdb as mdb
import re           #regular expressions
import threading    #threads
from threading import Lock
import logging      #
from collections import deque   # deque
import os           # clear screen
import datetime
import signal       # Ctrl+C
import calendar

# Database credentials
db_host = "localhost"
db_user = "alien"
db_pass = "EsR4SDiM1n"
db_db   = "alien"


def database_connection():
    return mdb.connect(db_host, db_user, db_pass, db_db)

# Logging
FORMAT = '%(asctime)s (%(threadName)-10s) %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format=FORMAT)

dbc = database_connection()

logging.info("Selecting list of all job_ids in t_jobs which have scrape_status=NULL.")

cur = dbc.cursor()
cur.execute("SELECT job_id FROM t_jobs WHERE scrape_status IS NULL ORDER BY job_id DESC")
job_ids = cur.fetchall()

logging.info("%d rows are to be updated." % len(job_ids))

N=0
for i_job_id in job_ids:
    cur = dbc.cursor()
    b = True
    while b:
        try:
            cur.callproc("p_update_job_details", [ i_job_id ])
            proc_result = cur.fetchone()
            b = False
        except (mdb.Error, mdb.Warning) as e:
            logging.warning("WARNING: MySQL error occurred with JobID %s. Trying again. Full error message: %s" % (i_job_id, str(e)))
            time.sleep(3)

    cur.close()
    N+=1

    if N%1==0: # Log a message only show every 1000 lines
        dbc.commit()
        logging.debug("Processed %d of %d rows. i_job_id=%s" % (N, len(job_ids), i_job_id))

logging.debug("Processed %d of %d rows" % (N, len(job_ids)))

logging.debug("Done.")
