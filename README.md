# AliEn-scraper

## Description

AliEn-scraper is a tool that continuously queries the central AliEn database for job transitions of jobIDs that have been/are running at a given AliEn site, writes them to a MySQL/MariaDB database where categorization and aggregation is performed.
The resulting data can be plotted using a suitable tool such as Grafana.

## Motivation

In some situations it can be desirable to know whether the jobs failing at your AliEn site do so due to a local malfunction or whether they have site-unspecific systematic problems.
Identifying how those exact jobs have run elsewhere, provided they have been resubmitted, can be an indication for that.

## Metrics

This application will download the list of JobIDs that have run at your site (saved to table `t_job`).
Subsequently, it will download the transitions (ASSIGNED, STARTED, RUNNING, ...) for each JobID until it has succeeded somewhere, or hasn't gotten any new transition for a certain amount of time. These transitions are saved to table `t_job_transitions`.

In a first step of aggregation, the following 4 numbers will be computed for each JobID.

ds := number of times a job has succeeded (**D**ONE) at your **s**ite

fs := number of times a job has **f**ailed at your **s**ite

do := number of times a job has succeeded (**D**ONE) at some **o**ther site

fo := number of times a job has **f**ailed at some **o**ther site


When treating ds, fs, do, fo as bits which are 1 if the number is greater than 0 and 0 else, each JobID can be categorized in exactly one out of 2^4=16 categories.

| cat     | ds  | fs  | do  | fo  | description                           |
| :-----: | :-: | :-: | :-: | :-: | :------------------------------------ |
| 0       | 0   | 0   | 0   | 0   | Didn't finish (yet)                   |
| 1       | 0   | 0   | 0   | 1   | Failed elsewhere                      |
| 2       | 0   | 0   | 1   | 0   | Succeeded elsewhere                   |
| 3       | 0   | 0   | 1   | 1   | Failed and succeeded elsewhere        |
| 4       | 0   | 1   | 0   | 0   | Failed here                           |
| 5       | 0   | 1   | 0   | 1   | Failed generally                      |
| 6       | 0   | 1   | 1   | 0   | Failed here, succeeded elsewhere      |
| 7       | 0   | 1   | 1   | 1   | Failed generally, succeeded elsewhere |
| 8       | 1   | 0   | 0   | 0   | Succeeded here                        |
| 9       | 1   | 0   | 0   | 1   | Failed elsewhere, succeeded here      |
| 10      | 1   | 0   | 1   | 0   | Succeeded here and elsewhere          |
| 11      | 1   | 0   | 1   | 1   | Failed elsewhere, succeeded generally |
| 12      | 1   | 1   | 0   | 0   | Failed and succeeded here             |
| 13      | 1   | 1   | 0   | 1   | Failed generally, succeeded here      |
| 14      | 1   | 1   | 1   | 0   | Failed here, succeeded generally      |
| 15      | 1   | 1   | 1   | 1   | Failed and succeeded generally        |

In a second step of aggregation, calendar day and hour histograms will be created by the stored procedure `p_update_histogram` and written into table `t_c_category_data` that specify:
 * how many JobIDs have fallen into the aforementioned categories
 * how much walltime was spent on succeeding/failing jobs here/elsewhere
 * KPIs such as T1 (percentage of cat6 jobIDs) and S2 (100.* the ratio of cat9 over cat6 jobIDs)

These histograms contain sufficiently aggregated data ideal for plotting with tools such as Grafana.

## Block diagram, internal mechanics, data flow

![Block Diagram](./doc/images/block-diagram.png)

![Internal mechanics](./doc/images/block-diagram-details.png)

## JobID scrape priority logic

JobIDs are ordered for scraping the following way:

`t_jobs` has a column `time_next_scrape`. It contains a hard datetime at which the JobID will be scraped (again) at the earliest. If the value is `NULL`, it will not be scraped again.
`t_jobs` also has a column `scrape_status` that will contain a number indicating whether the JobID will be scraped again (<100) or is considered given up (>2000).

When a JobID is seen the first time using `scr_top`, `time_next_scrape` will be set to `time_top`, the time when it was seen using the top scraper, indicating it can be scraped right away using `scr_ps`.

When a JobID is seen as currently running, it makes sense to wait some amount of time until scraping the JobID again.
Also, when a JobID is seen ending in an ERROR state. It may get resubmitted later, so giving it another scrape may be useful.

Definitions:

`time_top` := Time when JobID was inserted from `scraper_top`

`time_ps`  := Time when JobID was last scraped by `scraper_ps`

`T_S1`  := Time difference since JobID was initially scraped using scr_top (NOW - `t_jobs.time_top`)

`T_S2`  := Time difference since JobID had its last transition (NOW - `t.jobs.c_latest_t`)

`T_TNF` := Maximum time difference elapsed since the last transition of a JobID last seen in a non-final state before we abandon it

`T_TBF` := Maximum time difference elapsed since the last transition of a JobID last seen in a bad final state before we abandon it

`T_HNF` := Hold off time difference for JobIDs that have last been seen in a non-final state

`T_HBF` := Hold off time difference for JobIDs that have last been seen in a bad final state

The timeout and hold off parameters are configurable in `t_config`.

First matching condition wins:

| condition                                              | time_next_scrape | scrape_status  |
| :----------------------------------------------------- | :--------------: | -------------: |
| JobID was seen ending in DONE somewhere                | NULL             |           2050 |
| JobID has never been scraped before (time_ps is NULL)  | time_top         |             10 |
| JobID does not have any transitions && T_S1 <= T_TNF   | time_ps+T_HNF    |             31 |
| JobID does not have any transitions && T_S1 > T_TNF    | NULL             |           2031 |
| JobID last seen in a bad final-state && T_S2 <= T_TBF  | time_ps+T_HBF    |             40 |
| JobID last seen in a bad final state && T_S2 >  T_TBF  | NULL             |           2040 |
| JobID last seen in a non-final-state && T_S2 <= T_TNF  | time_ps+T_HNF    |             30 |
| JobID last seen in a non-final-state && T_S2 >  T_TNF  | NULL             |           2030 |

Refer to the procedure `p_update_job_details` for the actual implementation.

## Requirements

 * A working AliEn instance which allows to run `alien login`
 * A working AliEn proxy file as created by `alien proxy-init`
 * `mariadb`
 * `python2`
 * `mysql-python`
 * Optional: `grafana`

## Installation

### Manual installation

1. Install the dependencies specified above.
2. Create a MySQL/MariaDB database. In this example, the database is called `alien`. I recommend placing the directory of the database files on an SSD formatted with f2fs.
3. Create the necessary tables, views, functions, procedures by piping `tables_views_functions_procedures.sql` into your database (change database name accordingly).
4. Create a MySQL/MariaDB user with password and give See `users.sql`.

`$ mysql -u root -p --database=alien < tables_views_functions_procedures.sql`
5. Specify in the database the site you want to scrape:

`UPDATE t_config SET value='ALICE::GSI::GSI' WHERE keyname='site';`
6. Clone this repository.
7. Edit `scraper.conf` and specify your database credentials, the location of the AliEn executable, and the desired number of threads for the different scraping tasks. If unsure, leave the default.

## Usage
