-- This SQL file needs to be executed initially on the MariaDB/MySQL server
-- to initially create the necessary tables (some with rows), views, functions, procedures.
-- It should be idempotent, i.e. can be executed multiple times without failing.
-- Existing tables will be kept.
-- Existing views, functions, procedures will be dropped and recreated.

-- Tables
CREATE TABLE IF NOT EXISTS `t_jobs` (
  `job_id` BIGINT NOT NULL,
  `master_job_id` BIGINT DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `command` varchar(255) DEFAULT NULL,
  `site` varchar(100) NOT NULL,
  `time_top` datetime DEFAULT NULL,
  `time_ps` datetime DEFAULT NULL,
  `c_ds` int(11) NOT NULL DEFAULT 0,
  `c_fs` int(11) NOT NULL DEFAULT 0,
  `c_do` int(11) NOT NULL DEFAULT 0,
  `c_fo` int(11) NOT NULL DEFAULT 0,
  `c_t_ds` BIGINT NOT NULL DEFAULT 0,
  `c_t_fs` BIGINT NOT NULL DEFAULT 0,
  `c_t_do` BIGINT NOT NULL DEFAULT 0,
  `c_t_fo` BIGINT NOT NULL DEFAULT 0,
  `c_category` int(11) NOT NULL DEFAULT 0,
  `c_latest_id` BIGINT DEFAULT NULL,
  `c_latest_t` datetime DEFAULT NULL,
  `c_latest_site` varchar(255) DEFAULT NULL,
  `c_latest_state` varchar(255) DEFAULT NULL,
  `c_latest_worker_node` varchar(255) DEFAULT NULL,
  `c_latest_id_site` BIGINT DEFAULT NULL,
  `c_latest_t_site` datetime DEFAULT NULL,
  `c_latest_state_site` varchar(255) DEFAULT NULL,
  `c_latest_worker_node_site` varchar(255) DEFAULT NULL,
  `c_t1_site` datetime DEFAULT NULL,
  `c_t2_site` datetime DEFAULT NULL,
  `c_updated` datetime DEFAULT NULL,
  `time_next_scrape` datetime DEFAULT NULL,
  `scrape_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  KEY `i_master_job_id` (`master_job_id`),
  KEY `i_site` (`site`),
  KEY `i_time_top` (`time_top`),
  KEY `i_time_ps` (`time_ps`),
  KEY `i_user` (`user`),
  KEY `i_command` (`command`),
  KEY `i_c_ds` (`c_ds`),
  KEY `i_c_fs` (`c_fs`),
  KEY `i_c_do` (`c_do`),
  KEY `i_c_fo` (`c_fo`),
  KEY `i_c_category` (`c_category`),
  KEY `i_c_latest_t` (`c_latest_t`),
  KEY `i_c_latest_site` (`c_latest_site`),
  KEY `i_c_latest_worker_node` (`c_latest_worker_node`),
  KEY `i_c_latest_t_site` (`c_latest_t_site`),
  KEY `i_c_latest_worker_node_site` (`c_latest_worker_node_site`),
  KEY `i_c_updated` (`c_updated`),
  KEY `i_t1_site` (`c_t1_site`),
  KEY `i_t2_site` (`c_t2_site`),
  KEY `i_c_latest_id` (`c_latest_id`),
  KEY `i_c_latest_state` (`c_latest_state`),
  KEY `i_c_latest_id_site` (`c_latest_id_site`),
  KEY `i_c_latest_state_site` (`c_latest_state_site`),
  KEY `i_time_next_scrape` (`time_next_scrape`),
  KEY `i_scrape_status` (`scrape_status`),
  KEY `i_c_t_ds` (`c_t_ds`),
  KEY `i_c_t_fs` (`c_t_fs`),
  KEY `i_c_t_do` (`c_t_do`),
  KEY `i_c_t_fo` (`c_t_fo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_job_transitions` (
  `transition_id` BIGINT NOT NULL AUTO_INCREMENT,
  `job_id` BIGINT NOT NULL,
  `t` datetime NOT NULL,
  `alien_line_number` int(11) NOT NULL,
  `site` varchar(255) DEFAULT NULL,
  `speculative_site` tinyint(4) DEFAULT NULL,
  `speculative_worker_node` tinyint(4) DEFAULT NULL,
  `new_state` varchar(255) CHARACTER SET utf8 NOT NULL,
  `worker_node` varchar(255) DEFAULT NULL,
  `jobagent_id` varchar(255) DEFAULT NULL,
  `slurm_job_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`transition_id`),
  UNIQUE KEY `u_job_id_t_alien_line_number` (`job_id`,`t`,`alien_line_number`),
  KEY `i_t` (`t`),
  KEY `i_site` (`site`),
  KEY `i_new_state` (`new_state`),
  KEY `i_worker_node` (`worker_node`),
  KEY `i_jobagent_id` (`jobagent_id`),
  KEY `i_speculative_site` (`speculative_site`),
  KEY `i_speculative_worker_node` (`speculative_worker_node`),
  KEY `i_job_id` (`job_id`),
  KEY `i_alien_line_number` (`alien_line_number`),
  CONSTRAINT `fk_job_transitions_job_id` FOREIGN KEY (`job_id`) REFERENCES `t_jobs` (`job_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_master_jobs` (
  `master_job_id` BIGINT NOT NULL,
  `status` varchar(255) NOT NULL,
  `time_masterjob` datetime NOT NULL,
  `master_job_gone` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`master_job_id`),
  KEY `i_time_masterjob` (`time_masterjob`),
  KEY `i_status` (`status`),
  KEY `i_master_job_gone` (`master_job_gone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_master_job_numbers` (
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `master_job_id` BIGINT NOT NULL,
  `site` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `number` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`entry_id`),
  KEY `i_master_job_id` (`master_job_id`),
  KEY `i_site` (`site`),
  KEY `i_number` (`number`),
  KEY `i_status` (`status`),
  CONSTRAINT `fk_master_job_numbers_master_job_id` FOREIGN KEY (`master_job_id`) REFERENCES `t_master_jobs` (`master_job_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_c_category_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interval_type` varchar(4) DEFAULT NULL,
  `t_start` datetime NOT NULL,
  `t_end` datetime NOT NULL,
  `cat0` int(11) NOT NULL DEFAULT 0,
  `cat1` int(11) NOT NULL DEFAULT 0,
  `cat2` int(11) NOT NULL DEFAULT 0,
  `cat3` int(11) NOT NULL DEFAULT 0,
  `cat4` int(11) NOT NULL DEFAULT 0,
  `cat5` int(11) NOT NULL DEFAULT 0,
  `cat6` int(11) NOT NULL DEFAULT 0,
  `cat7` int(11) NOT NULL DEFAULT 0,
  `cat8` int(11) NOT NULL DEFAULT 0,
  `cat9` int(11) NOT NULL DEFAULT 0,
  `cat10` int(11) NOT NULL DEFAULT 0,
  `cat11` int(11) NOT NULL DEFAULT 0,
  `cat12` int(11) NOT NULL DEFAULT 0,
  `cat13` int(11) NOT NULL DEFAULT 0,
  `cat14` int(11) NOT NULL DEFAULT 0,
  `cat15` int(11) NOT NULL DEFAULT 0,
  `total` int(11) NOT NULL DEFAULT 0,
  `t_ds` bigint(20) NOT NULL DEFAULT 0,
  `t_fs` bigint(20) NOT NULL DEFAULT 0,
  `t_do` bigint(20) NOT NULL DEFAULT 0,
  `t_fo` bigint(20) NOT NULL DEFAULT 0,
  `kpi_k1` decimal(18,2) DEFAULT NULL,
  `kpi_k2` decimal(18,2) DEFAULT NULL,
  `kpi_s2` decimal(18,2) DEFAULT NULL,
  `kpi_t1` decimal(18,2) DEFAULT NULL,
  `kpi_t1_1` decimal(18,2) DEFAULT NULL,
  `t_updated` datetime NOT NULL,
  `t_outdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_interval_type_t_start` (`interval_type`,`t_start`),
  KEY `i_t_start` (`t_start`),
  KEY `i_t_end` (`t_end`),
  KEY `i_t_outdated` (`t_outdated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_c_category_data_wns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interval_type` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wn` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `t_start` datetime NOT NULL,
  `t_end` datetime NOT NULL,
  `cat0` int(11) NOT NULL DEFAULT 0,
  `cat1` int(11) NOT NULL DEFAULT 0,
  `cat2` int(11) NOT NULL DEFAULT 0,
  `cat3` int(11) NOT NULL DEFAULT 0,
  `cat4` int(11) NOT NULL DEFAULT 0,
  `cat5` int(11) NOT NULL DEFAULT 0,
  `cat6` int(11) NOT NULL DEFAULT 0,
  `cat7` int(11) NOT NULL DEFAULT 0,
  `cat8` int(11) NOT NULL DEFAULT 0,
  `cat9` int(11) NOT NULL DEFAULT 0,
  `cat10` int(11) NOT NULL DEFAULT 0,
  `cat11` int(11) NOT NULL DEFAULT 0,
  `cat12` int(11) NOT NULL DEFAULT 0,
  `cat13` int(11) NOT NULL DEFAULT 0,
  `cat14` int(11) NOT NULL DEFAULT 0,
  `cat15` int(11) NOT NULL DEFAULT 0,
  `total` int(11) NOT NULL,
  `t_ds` bigint(20) NOT NULL DEFAULT 0,
  `t_fs` bigint(20) NOT NULL DEFAULT 0,
  `t_do` bigint(20) NOT NULL DEFAULT 0,
  `t_fo` bigint(20) NOT NULL DEFAULT 0,
  `kpi_k1` decimal(18,2) DEFAULT NULL,
  `kpi_k2` decimal(18,2) DEFAULT NULL,
  `kpi_s2` decimal(18,2) DEFAULT NULL,
  `kpi_t1` decimal(18,2) DEFAULT NULL,
  `t_updated` datetime NOT NULL,
  `t_outdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_interval_type_wn_t_start` (`interval_type`,`wn`,`t_start`),
  KEY `i_t_start` (`t_start`),
  KEY `i_t_end` (`t_end`),
  KEY `i_interval_type_wn_t_start` (`interval_type`,`wn`,`t_start`),
  KEY `i_t_outdated` (`t_outdated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `t_config` (
  `keyname` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  UNIQUE KEY `u1` (`keyname`,`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ERROR_E');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ERROR_EW');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ERROR_IB');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ERROR_RE');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ERROR_SV');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ERROR_V');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ERROR_VN');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ERROR_S');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'EXPIRED');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'Killed: it was running for longer than its TTL');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'Killed: not existing queueId-token set anymore');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_bad', 'ZOMBIE');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_good', 'DONE');
INSERT IGNORE INTO t_config (keyname, value) VALUES('final_state_good', 'DONE_WARN');
INSERT IGNORE INTO t_config (keyname, value) VALUES('nonfinal_state', 'ASSIGNED');
INSERT IGNORE INTO t_config (keyname, value) VALUES('nonfinal_state', 'RUNNING');
INSERT IGNORE INTO t_config (keyname, value) VALUES('nonfinal_state', 'SAVED');
INSERT IGNORE INTO t_config (keyname, value) VALUES('nonfinal_state', 'SAVING');
INSERT IGNORE INTO t_config (keyname, value) VALUES('nonfinal_state', 'WAITING');
INSERT IGNORE INTO t_config (keyname, value) VALUES('T_HBF_hours', 1);
INSERT IGNORE INTO t_config (keyname, value) VALUES('T_HNF_hours', 1);
INSERT IGNORE INTO t_config (keyname, value) VALUES('T_TBF_hours', 96);
INSERT IGNORE INTO t_config (keyname, value) VALUES('T_TNF_hours', 96);
INSERT IGNORE INTO t_config (keyname, value) VALUES('site', 'ALICE::YOUR_SITE_HERE::YOUR_SITE_HERE');

CREATE TABLE IF NOT EXISTS `t_config_categories` (
  `category_id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(0, 'Did not finish (yet)');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(1, 'Failed elsewhere');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(2, 'Succeeded elsewhere');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(3, 'Failed and succeeded elsewhere');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(4, 'Failed here');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(5, 'Failed generally');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(6, 'Failed here, succeeded elsewhere');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(7, 'Failed generally, succeeded elsewhere');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(8, 'Succeeded here');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(9, 'Failed elsewhere, succeeded here');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(10, 'Succeeded here and elsewhere');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(11, 'Failed elsewhere, succeeded generally');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(12, 'Failed and succeeded here');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(13, 'Failed generally, succeeded here');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(14, 'Failed here, succeeded generally');
INSERT IGNORE INTO t_config_categories(category_id, description) VALUES(15, 'Failed and succeeded generally');

CREATE TABLE IF NOT EXISTS `t_config_walltimes` (
  `range_id` int(11) NOT NULL AUTO_INCREMENT,
  `wn_from` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wn_to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hs06` decimal(18,5) DEFAULT NULL,
  `num_cores` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`range_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_annotations` (
  `annotation_id` int(11) NOT NULL AUTO_INCREMENT,
  `t` datetime NOT NULL,
  `annotation_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annotation_tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`annotation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_config_pledges` (
  `calendar_year` int(11) NOT NULL,
  `walltime_pledge_seconds` BIGINT NOT NULL,
  PRIMARY KEY (`calendar_year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Views

DROP VIEW IF EXISTS v_queue_masterjobs;
CREATE VIEW v_queue_masterjobs AS
SELECT
DISTINCT j.master_job_id,
1 AS f_needs_scraping_masterjob
FROM t_jobs AS j
LEFT JOIN t_master_jobs AS mj ON j.master_job_id=mj.master_job_id
WHERE IFNULL(mj.status,"")!="DONE"
AND IFNULL(mj.master_job_gone,0)=0
AND j.master_job_id>0;

DROP VIEW IF EXISTS v_queue_histograms_day;
CREATE VIEW v_queue_histograms_day AS
SELECT
  FROM_DAYS(s.seq) AS t,
  NULL AS wn
FROM seq_700000_to_1000000 AS s
LEFT JOIN t_c_category_data AS c
ON FROM_DAYS(s.seq) = c.t_start
AND c.interval_type = 'day'
WHERE s.seq BETWEEN (SELECT TO_DAYS(MIN(tr.t)) FROM t_job_transitions AS tr) AND TO_DAYS(NOW())
AND c.t_start IS NULL
UNION
SELECT
  t_start AS t,
  NULL AS wn
FROM t_c_category_data
WHERE interval_type = 'day'
AND t_outdated IS NOT NULL;

DROP VIEW IF EXISTS v_queue_histograms_hour;
CREATE VIEW v_queue_histograms_hour AS
SELECT
  STR_TO_DATE(DATE_FORMAT(CONCAT(FROM_DAYS(s.seq), " ", h.seq, ":00:00"), "%Y-%m-%d %H:00:00"),'%Y-%m-%d %H:%i:%s') AS t,
  NULL AS wn
FROM seq_700000_to_1000000 AS s
CROSS JOIN seq_0_to_23 AS h
LEFT JOIN t_c_category_data AS c
ON STR_TO_DATE(DATE_FORMAT(CONCAT(FROM_DAYS(s.seq), " ", h.seq, ":00:00"), "%Y-%m-%d %H:00:00"),'%Y-%m-%d %H:%i:%s') = c.t_start
AND c.interval_type = 'hour'
WHERE s.seq BETWEEN (SELECT TO_DAYS(MIN(tr.t)) FROM t_job_transitions AS tr) AND TO_DAYS(NOW())
AND c.t_start IS NULL
AND STR_TO_DATE(DATE_FORMAT(CONCAT(FROM_DAYS(s.seq), " ", h.seq, ":00:00"), "%Y-%m-%d %H:00:00"),'%Y-%m-%d %H:%i:%s')<=NOW()
UNION
SELECT
  t_start AS t,
  NULL AS wn
FROM t_c_category_data
WHERE interval_type = 'hour'
AND t_outdated IS NOT NULL;

DROP VIEW IF EXISTS v_walltimes;
CREATE VIEW v_walltimes AS
SELECT
h.t_start,
c.range_id,
SUM(t_ds) AS sum_t_ds,
SUM(t_fs) AS sum_t_fs,
SUM(t_ds)*c.hs06*c.num_cores/TIMESTAMPDIFF(SECOND, t_start, t_start + INTERVAL 1 DAY - INTERVAL 1 SECOND) AS sum_hs06_ds,
SUM(t_fs)*c.hs06*c.num_cores/TIMESTAMPDIFF(SECOND, t_start, t_start + INTERVAL 1 DAY - INTERVAL 1 SECOND) AS sum_hs06_fs
FROM t_c_category_data_wns AS h
CROSS JOIN t_config_walltimes AS c
WHERE t_start BETWEEN CONCAT(t_start," 00:00:00") AND CONCAT(t_start, " 23:59:59")
AND interval_type="day"
AND wn BETWEEN c.wn_from AND c.wn_to
GROUP BY t_start;


DROP VIEW IF EXISTS v_walltimes_1;
CREATE VIEW v_walltimes_1 AS
SELECT
t_start,
sum_hs06_ds AS sum_hs06_ds_1,
sum_hs06_fs AS sum_hs06_fs_1,
NULL AS sum_hs06_ds_2,
NULL AS sum_hs06_fs_2,
NULL AS sum_hs06_ds_3,
NULL AS sum_hs06_fs_3,
NULL AS sum_hs06_ds_4,
NULL AS sum_hs06_fs_4
FROM v_walltimes AS v
WHERE range_id=1
UNION ALL
SELECT
t_start,
NULL AS sum_hs06_ds_1,
NULL AS sum_hs06_fs_1,
sum_hs06_ds AS sum_hs06_ds_2,
sum_hs06_fs AS sum_hs06_fs_2,
NULL AS sum_hs06_ds_3,
NULL AS sum_hs06_fs_3,
NULL AS sum_hs06_ds_4,
NULL AS sum_hs06_fs_4
FROM v_walltimes AS v
WHERE range_id=2
UNION ALL
SELECT
t_start,
NULL AS sum_hs06_ds_1,
NULL AS sum_hs06_fs_1,
NULL AS sum_hs06_ds_2,
NULL AS sum_hs06_fs_2,
sum_hs06_ds AS sum_hs06_ds_3,
sum_hs06_fs AS sum_hs06_fs_3,
NULL AS sum_hs06_ds_4,
NULL AS sum_hs06_fs_4
FROM v_walltimes AS v
WHERE range_id=3
UNION ALL
SELECT
t_start,
NULL AS sum_hs06_ds_1,
NULL AS sum_hs06_fs_1,
NULL AS sum_hs06_ds_2,
NULL AS sum_hs06_fs_2,
NULL AS sum_hs06_ds_3,
NULL AS sum_hs06_fs_3,
sum_hs06_ds AS sum_hs06_ds_4,
sum_hs06_fs AS sum_hs06_fs_4
FROM v_walltimes AS v
WHERE range_id=4;

-- Functions
DROP FUNCTION IF EXISTS f_results_kpi_t1_direct;
DELIMITER //
CREATE FUNCTION f_results_kpi_t1_direct
(
  in_t1 DATETIME,
  in_t2 DATETIME
)
RETURNS DECIMAL(18,2)
READS SQL DATA
BEGIN
  DECLARE num_jobs_total INT;
  DECLARE num_jobs_cat6 INT;
  DECLARE x DECIMAL(18,2);

  SET num_jobs_total = (SELECT COUNT(*) FROM t_jobs WHERE NOT c_t2_site<in_t1 AND NOT c_t1_site>in_t2);
  SET num_jobs_cat6 = (SELECT COUNT(*) FROM t_jobs WHERE NOT c_t2_site<in_t1 AND NOT c_t1_site>in_t2 AND c_category=6);
  SET x = 100.*num_jobs_cat6/num_jobs_total;

  RETURN x;
END
//
DELIMITER ;



DROP FUNCTION IF EXISTS f_results_kpi_t1;
DELIMITER //
CREATE FUNCTION f_results_kpi_t1
(
  in_t1 DATETIME,
  in_t2 DATETIME
)
RETURNS DECIMAL(18,2)
READS SQL DATA
BEGIN
  DECLARE num_jobs_total INT;
  DECLARE num_jobs_cat6 INT;
  DECLARE x DECIMAL(18,2);

  SET num_jobs_total = (SELECT SUM(total) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_jobs_cat6 = (SELECT SUM(cat6) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET x = 100.*num_jobs_cat6/num_jobs_total;

  RETURN x;
END
//
DELIMITER ;


DROP FUNCTION IF EXISTS f_results_kpi_s2_direct;
DELIMITER //
CREATE FUNCTION f_results_kpi_s2_direct
(
  in_t1 DATETIME,
  in_t2 DATETIME
)
RETURNS DECIMAL(18,2)
READS SQL DATA
BEGIN
  DECLARE num_jobs_cat6 INT;
  DECLARE num_jobs_cat9 INT;
  DECLARE x DECIMAL(18,2);

  SET num_jobs_cat6 = (SELECT COUNT(*) FROM t_jobs WHERE NOT c_t2_site<in_t1 AND NOT c_t1_site>in_t2 AND c_category=6);
  SET num_jobs_cat9 = (SELECT COUNT(*) FROM t_jobs WHERE NOT c_t2_site<in_t1 AND NOT c_t1_site>in_t2 AND c_category=9);
  SET x = 100.*num_jobs_cat9/num_jobs_cat6;

  RETURN x;
END
//
DELIMITER ;



DROP FUNCTION IF EXISTS f_results_kpi_s2;
DELIMITER //
CREATE FUNCTION f_results_kpi_s2
(
  in_t1 DATETIME,
  in_t2 DATETIME
)
RETURNS DECIMAL(18,2)
READS SQL DATA
BEGIN
  DECLARE num_jobs_cat6 INT;
  DECLARE num_jobs_cat9 INT;
  DECLARE x DECIMAL(18,2);

  SET num_jobs_cat9 = (SELECT SUM(cat9) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_jobs_cat6 = (SELECT SUM(cat6) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);

  SET x = 100.*num_jobs_cat9/num_jobs_cat6;

  RETURN x;
END
//
DELIMITER ;


-- Procedures
DROP PROCEDURE IF EXISTS p_scraper_top;
DELIMITER //
CREATE PROCEDURE p_scraper_top
(
  IN in_job_id BIGINT,
  IN in_user VARCHAR(50),
  IN in_command VARCHAR(255),
  IN in_site VARCHAR(100),
  OUT out_return_text VARCHAR(255),
  OUT out_return_code INT
)
MODIFIES SQL DATA
BEGIN
  # PROCEDURE p_scraper_top
  #
  # Receives data from the top scraper. This procedure is run for each row.
  # This procedure is transaction-safe iff the START TRANSACTION/COMMIT statement is in use.
  # However, this is extremely slow.
  # Comment out the START TRANSACTION/COMMIT statements for a much faster execution
  # if you can guarantee that only one instance of the scraper script will run at a given time.
  #
  # in_user, in_command cannot be NULL.
  #
  # Return codes <0 are errors, >0 are successfull ones.

  SET out_return_code = 0;
  SET out_return_text = "";


  # Check supplied parameters
  IF in_job_id IS NULL THEN
    SET out_return_text="ERROR: p_scraper_top: in_job_id cannot be NULL\n";
    SET out_return_code = -1;
  END IF;

  IF in_user IS NULL THEN
    SET out_return_text=CONCAT(out_return_text,"ERROR: p_scraper_top: in_user cannot be NULL\n");
    SET out_return_code = -1;
  END IF;

  IF in_command IS NULL THEN
    SET out_return_text=CONCAT(out_return_text,"ERROR: p_scraper_top: in_command cannot be NULL\n");
    SET out_return_code = -1;
  END IF;

  IF in_site IS NULL THEN
    SET out_return_text=CONCAT(out_return_text,"ERROR: p_scraper_top: in_site cannot be NULL\n");
    SET out_return_code = -1;
  END IF;

  # Only proceed if there have not been any errors so far.
  IF out_return_code >=0 THEN
    INSERT IGNORE INTO t_jobs(
      job_id,
      user,
      command,
      site,
      time_top,
      time_next_scrape,
      scrape_status
      )
      VALUES(
      in_job_id,
      in_user,
      in_command,
      in_site,
      NOW(),
      NOW(),
      10
    );
    SET out_return_text = "p_scraper_top: Row processed successfully.";
    SET out_return_code = 3;

  END IF; # Only proceed if there have not been any errors so far.

  SELECT out_return_code, out_return_text;

END; #PROCEDURE
//
DELIMITER ;


DROP PROCEDURE IF EXISTS p_update_job_details;
DELIMITER //
CREATE PROCEDURE p_update_job_details
(
  IN in_job_id BIGINT
)
MODIFIES SQL DATA
BEGIN
  # PROCEDURE p_update_job_details
  #
  # Updates the cache columns t_jobs.c_* with the data from t_job_transitions.
  # This procedure needs to be called by scraper_ps after all necessary transitions for a given JobID are inserted.

  DECLARE ds INT DEFAULT 0;
  DECLARE fs INT DEFAULT 0;
  DECLARE do INT DEFAULT 0;
  DECLARE fo INT DEFAULT 0;
  DECLARE category INT DEFAULT 0;
  DECLARE latest_id BIGINT DEFAULT 0;
  DECLARE latest_t DATETIME DEFAULT NULL;
  DECLARE latest_site VARCHAR(255);
  DECLARE latest_state VARCHAR(255);
  DECLARE latest_worker_node VARCHAR(255);
  DECLARE latest_id_site BIGINT DEFAULT 0;
  DECLARE latest_t_site DATETIME DEFAULT NULL;
  DECLARE latest_state_site VARCHAR(255);
  DECLARE latest_worker_node_site VARCHAR(255);
  DECLARE t1_site DATETIME;
  DECLARE t2_site DATETIME;
  DECLARE x_site VARCHAR(255);
  DECLARE x_final_states_good VARCHAR(4096);
  DECLARE x_final_states_bad VARCHAR(4096);
  DECLARE x_nonfinal_states VARCHAR(4096);
  DECLARE x_T_HBF_hours INT;
  DECLARE x_T_HNF_hours INT;
  DECLARE x_T_TBF_hours INT;
  DECLARE x_T_TNF_hours INT;

  DECLARE x_time_top DATETIME;
  DECLARE x_time_ps DATETIME;
  DECLARE x_c_latest_state VARCHAR(255);
  DECLARE x_c_latest_t DATETIME;
  DECLARE x_time_next_scrape DATETIME DEFAULT NULL;
  DECLARE x_scrape_status INT DEFAULT NULL;

  DECLARE x_old_category INT;
  DECLARE x_old_t DATETIME;
  DECLARE x_old_wn VARCHAR(255);

  DECLARE x_t_start DATETIME;
  DECLARE x_t_end DATETIME;

  # Walltimes, specified in seconds
  DECLARE x_t_ds BIGINT DEFAULT 0; # Walltime spent at our site resulting in a DONE job.
  DECLARE x_t_fs BIGINT DEFAULT 0; # Walltime spent at our site resulting in a failed job.
  DECLARE x_t_do BIGINT DEFAULT 0; # Walltime spent elsewhere resulting in a DONE job.
  DECLARE x_t_fo BIGINT DEFAULT 0; # Walltime spent elsewhere resultung in a failed job.

  # Walltimes before updating
  DECLARE x_old_t_ds BIGINT DEFAULT 0;
  DECLARE x_old_t_fs BIGINT DEFAULT 0;
  DECLARE x_old_t_do BIGINT DEFAULT 0;
  DECLARE x_old_t_fo BIGINT DEFAULT 0;

  # Helper variables for state machine
  DECLARE x_t_done INT DEFAULT FALSE;
  DECLARE x_t_t_start DATETIME; # Stores the start time of the run of which we are trying to determine the time spent
  DECLARE x_t_site VARCHAR(255) DEFAULT ""; # To make sure we dont switch sites
  DECLARE x_t_state VARCHAR(255) DEFAULT "st_look_for_start"; # state variable of the state machine to determine times spent
  DECLARE x_t_next_state VARCHAR(255) DEFAULT "";
  # Cursor and cursor helper variables
  DECLARE x_cur_t DATETIME;
  DECLARE x_cur_site VARCHAR(255) DEFAULT "";
  DECLARE x_cur_new_state VARCHAR(255) DEFAULT "";
  DECLARE x_cur CURSOR FOR SELECT t, site, new_state FROM t_job_transitions WHERE job_id=in_job_id;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET x_t_done = TRUE;

  SET x_site=(SELECT value FROM t_config WHERE keyname="site");
  SET x_final_states_good=(SELECT GROUP_CONCAT(value) FROM t_config WHERE keyname="final_state_good");
  SET x_final_states_bad =(SELECT GROUP_CONCAT(value) FROM t_config WHERE keyname="final_state_bad");
  SET x_nonfinal_states  =(SELECT GROUP_CONCAT(value) FROM t_config WHERE keyname="nonfinal_state");
  SET x_T_HBF_hours=(SELECT value FROM t_config WHERE keyname="T_HBF_hours");
  SET x_T_HNF_hours=(SELECT value FROM t_config WHERE keyname="T_HNF_hours");
  SET x_T_TBF_hours=(SELECT value FROM t_config WHERE keyname="T_TBF_hours");
  SET x_T_TNF_hours=(SELECT value FROM t_config WHERE keyname="T_TNF_hours");

  SET ds=(SELECT COUNT(*) FROM t_job_transitions WHERE job_id=in_job_id AND FIND_IN_SET(new_state, x_final_states_good) AND site =x_site);
  SET fs=(SELECT COUNT(*) FROM t_job_transitions WHERE job_id=in_job_id AND FIND_IN_SET(new_state, x_final_states_bad)  AND site =x_site);
  SET do=(SELECT COUNT(*) FROM t_job_transitions WHERE job_id=in_job_id AND FIND_IN_SET(new_state, x_final_states_good) AND site!=x_site);
  SET fo=(SELECT COUNT(*) FROM t_job_transitions WHERE job_id=in_job_id AND FIND_IN_SET(new_state, x_final_states_bad)  AND site!=x_site);
  SET category=(CASE
    WHEN (IFNULL(ds,0)=0 AND IFNULL(fs,0)=0 AND IFNULL(do,0)=0 AND IFNULL(fo,0)=0) THEN 0
    WHEN (IFNULL(ds,0)=0 AND IFNULL(fs,0)=0 AND IFNULL(do,0)=0 AND IFNULL(fo,0)>0) THEN 1
    WHEN (IFNULL(ds,0)=0 AND IFNULL(fs,0)=0 AND IFNULL(do,0)>0 AND IFNULL(fo,0)=0) THEN 2
    WHEN (IFNULL(ds,0)=0 AND IFNULL(fs,0)=0 AND IFNULL(do,0)>0 AND IFNULL(fo,0)>0) THEN 3
    WHEN (IFNULL(ds,0)=0 AND IFNULL(fs,0)>0 AND IFNULL(do,0)=0 AND IFNULL(fo,0)=0) THEN 4
    WHEN (IFNULL(ds,0)=0 AND IFNULL(fs,0)>0 AND IFNULL(do,0)=0 AND IFNULL(fo,0)>0) THEN 5
    WHEN (IFNULL(ds,0)=0 AND IFNULL(fs,0)>0 AND IFNULL(do,0)>0 AND IFNULL(fo,0)=0) THEN 6
    WHEN (IFNULL(ds,0)=0 AND IFNULL(fs,0)>0 AND IFNULL(do,0)>0 AND IFNULL(fo,0)>0) THEN 7
    WHEN (IFNULL(ds,0)>0 AND IFNULL(fs,0)=0 AND IFNULL(do,0)=0 AND IFNULL(fo,0)=0) THEN 8
    WHEN (IFNULL(ds,0)>0 AND IFNULL(fs,0)=0 AND IFNULL(do,0)=0 AND IFNULL(fo,0)>0) THEN 9
    WHEN (IFNULL(ds,0)>0 AND IFNULL(fs,0)=0 AND IFNULL(do,0)>0 AND IFNULL(fo,0)=0) THEN 10
    WHEN (IFNULL(ds,0)>0 AND IFNULL(fs,0)=0 AND IFNULL(do,0)>0 AND IFNULL(fo,0)>0) THEN 11
    WHEN (IFNULL(ds,0)>0 AND IFNULL(fs,0)>0 AND IFNULL(do,0)=0 AND IFNULL(fo,0)=0) THEN 12
    WHEN (IFNULL(ds,0)>0 AND IFNULL(fs,0)>0 AND IFNULL(do,0)=0 AND IFNULL(fo,0)>0) THEN 13
    WHEN (IFNULL(ds,0)>0 AND IFNULL(fs,0)>0 AND IFNULL(do,0)>0 AND IFNULL(fo,0)=0) THEN 14
    WHEN (IFNULL(ds,0)>0 AND IFNULL(fs,0)>0 AND IFNULL(do,0)>0 AND IFNULL(fo,0)>0) THEN 15
    ELSE 999
  END);

  SET latest_id=(SELECT transition_id FROM t_job_transitions WHERE job_id=in_job_id ORDER BY t DESC LIMIT 1);
  SET latest_t=(SELECT t FROM t_job_transitions WHERE transition_id=latest_id);
  SET latest_site=(SELECT site FROM t_job_transitions WHERE transition_id=latest_id);
  SET latest_state=(SELECT new_state FROM t_job_transitions WHERE transition_id=latest_id);
  SET latest_worker_node=(SELECT worker_node FROM t_job_transitions WHERE transition_id=latest_id);

  SET latest_id_site=(SELECT transition_id FROM t_job_transitions WHERE job_id=in_job_id AND site=(SELECT value FROM t_config WHERE keyname="site" LIMIT 1) ORDER BY t DESC LIMIT 1);
  SET latest_t_site=(SELECT t FROM t_job_transitions WHERE transition_id=latest_id_site);
  SET latest_state_site=(SELECT new_state FROM t_job_transitions WHERE transition_id=latest_id_site);
  SET latest_worker_node_site=(SELECT worker_node FROM t_job_transitions WHERE transition_id=latest_id_site);

  SET t1_site=(SELECT t FROM t_job_transitions WHERE job_id=in_job_id AND site=(SELECT value FROM t_config WHERE keyname="site" LIMIT 1) ORDER BY t ASC LIMIT 1);
  SET t2_site=(SELECT t FROM t_job_transitions WHERE job_id=in_job_id AND site=(SELECT value FROM t_config WHERE keyname="site" LIMIT 1) ORDER BY t DESC LIMIT 1);

  SET x_time_top=(SELECT time_top FROM t_jobs WHERE job_id=in_job_id);
  SET x_time_ps=(SELECT time_ps FROM t_jobs WHERE job_id=in_job_id);

  # Compute time_next_scrape and scrape_status
  CASE
    WHEN ds > 0 OR do > 0 THEN
      # JobID was seen DONE somewhere
      SET x_scrape_status = 2050;
      SET x_time_next_scrape = NULL;
    WHEN x_time_ps IS NULL THEN
      # JobID does not have a time_ps, so it was probably just scraped by scraper_top
      SET x_scrape_status = 10;
      SET x_time_next_scrape = x_time_top;
    WHEN latest_id IS NULL AND latest_t_site IS NULL AND TIMESTAMPDIFF(HOUR, x_time_top, NOW()) > x_T_TNF_hours THEN
      # JobID was scraped previously, did not have any transitions for x_T_TNF_hours hours, so we give up on it
      SET x_scrape_status = 2031;
      SET x_time_next_scrape = NULL;
    WHEN latest_id IS NULL AND TIMESTAMPDIFF(HOUR, x_time_ps, NOW()) <= x_T_TNF_hours THEN
      # JobID was scraped previously, but did not have any transitions, and we are not giving up on it yet
      SET x_scrape_status = 31;
      SET x_time_next_scrape = x_time_ps + INTERVAL x_T_HNF_hours HOUR;
    WHEN FIND_IN_SET(latest_state, x_final_states_bad) AND TIMESTAMPDIFF(HOUR, latest_t, NOW()) <= x_T_TBF_hours THEN
      # JobID last seen in a bad final state but we are not giving up yet
      SET x_scrape_status = 40;
      SET x_time_next_scrape = x_time_ps + INTERVAL x_T_HBF_hours HOUR;
    WHEN FIND_IN_SET(latest_state, x_final_states_bad) AND TIMESTAMPDIFF(HOUR, latest_t, NOW()) > x_T_TBF_hours THEN
      # JobID last seen in a bad final state but has not moved for over x_T_TBF_hours hours, so we are giving up on it
      SET x_scrape_status = 2040;
      SET x_time_next_scrape = NULL;
    # from here we assume that we are dealing with non-final or bogus states
    WHEN TIMESTAMPDIFF(HOUR, latest_t, NOW()) <= x_T_TNF_hours THEN
      # JobID last seen in a non-final or bogus state, but we are not giving up yet
      SET x_scrape_status = 30;
      SET x_time_next_scrape = x_time_ps + INTERVAL x_T_HNF_hours HOUR;
    WHEN TIMESTAMPDIFF(HOUR, latest_t, NOW()) > x_T_TNF_hours THEN
      # JobID last seen in a non-final or bogus state, but has not moved for over x_T_TNF_hours hours, so we give up on it
      SET x_scrape_status = 2030;
      SET x_time_next_scrape = NULL;
    ELSE
      # this should never match. If entries like this show up in the database, something is wrong.
      SET x_scrape_status = 9999;
      SET x_time_next_scrape = NULL;
  END CASE;

  SET x_old_category = (SELECT c_category FROM t_jobs WHERE job_id=in_job_id);
  SET x_old_t = (SELECT c_latest_t_site FROM t_jobs WHERE job_id=in_job_id);
  SET x_old_wn = (SELECT c_latest_worker_node_site FROM t_jobs WHERE job_id=in_job_id);

  # Remember old walltimes before updating them
  SELECT
    SUM(c_t_ds),
    SUM(c_t_fs),
    SUM(c_t_do),
    SUM(c_t_fo)
  FROM t_jobs AS j
  WHERE j.job_id=in_job_id
  INTO
    x_old_t_ds,
    x_old_t_fs,
    x_old_t_do,
    x_old_t_fo;

  # Compute walltimes spent
  OPEN x_cur;
  transitions_loop: LOOP
    FETCH x_cur INTO x_cur_t, x_cur_site, x_cur_new_state;
    IF x_t_done THEN
      LEAVE transitions_loop;
    END IF;
    IF x_t_state = "st_look_for_start" THEN
      IF x_cur_new_state = "RUNNING" THEN
        SET x_t_t_start = x_cur_t;
        SET x_t_site = x_cur_site;
        SET x_t_next_state = "st_look_for_end";
      ELSE
        SET x_t_next_state = "st_look_for_start";
      END IF;

    ELSEIF x_t_state = "st_look_for_end" THEN
      # We still need to check for a start, there might be errorneous consecutive RUNNING entries
      IF x_cur_new_state = "RUNNING" THEN
        SET x_t_t_start = x_cur_t;
        SET x_t_site = x_cur_site;
        SET x_t_next_state = "st_look_for_end";
      ELSE # something other than RUNNING
        IF FIND_IN_SET(x_cur_new_state, x_final_states_good) THEN # found a DONE job
          IF x_t_site = x_cur_site THEN # site stayed the same since we encountered RUNNING
            IF x_t_site = x_site THEN
              SET x_t_ds = x_t_ds + TIMESTAMPDIFF(SECOND, x_t_t_start, x_cur_t);
            ELSE
              SET x_t_do = x_t_do + TIMESTAMPDIFF(SECOND, x_t_t_start, x_cur_t);
            END IF;
          END IF;
          SET x_t_next_state = "st_look_for_start";
        ELSEIF FIND_IN_SET(x_cur_new_state, x_final_states_bad) THEN # found a failed job
          IF x_t_site = x_cur_site THEN # site stayed the same since we encountered RUNNING
            IF x_t_site = x_site THEN
              SET x_t_fs = x_t_fs + TIMESTAMPDIFF(SECOND, x_t_t_start, x_cur_t);
            ELSE
              SET x_t_fo = x_t_fo + TIMESTAMPDIFF(SECOND, x_t_t_start, x_cur_t);
            END IF;
          END IF;
          SET x_t_next_state = "st_look_for_start";
        END IF; # FIND_IN_SET

        SET x_t_next_state = "st_look_for_end";

      END IF; # transitions

    END IF; # states
    SET x_t_state = x_t_next_state;
  END LOOP;
  CLOSE x_cur;

  UPDATE t_jobs SET
    c_ds=ds,
    c_fs=fs,
    c_do=do,
    c_fo=fo,
    c_t_ds=x_t_ds,
    c_t_fs=x_t_fs,
    c_t_do=x_t_do,
    c_t_fo=x_t_fo,
    c_category=category,
    c_latest_id=latest_id,
    c_latest_t=latest_t,
    c_latest_site=latest_site,
    c_latest_state=latest_state,
    c_latest_worker_node=latest_worker_node,
    c_latest_id_site=latest_id_site,
    c_latest_t_site=latest_t_site,
    c_latest_state_site=latest_state_site,
    c_latest_worker_node_site=latest_worker_node_site,
    c_t1_site=t1_site,
    c_t2_site=t2_site,
    c_updated=NOW(),
    time_next_scrape=x_time_next_scrape,
    scrape_status=x_scrape_status
  WHERE job_id=in_job_id;

  IF x_old_category != category OR x_old_t_ds != x_t_ds OR x_old_t_fs != x_t_fs OR x_old_t_do != x_t_do OR x_old_t_fo != x_t_fo THEN
    -- If the category or any of the walltimes of this JobID have changed,
    -- mark those historgrams as outdated which refer to a time interval in which our JobID was last seen at our site.
    -- That is the histogram where this JobID now appears in.
    UPDATE t_c_category_data SET t_outdated=NOW() WHERE latest_t_site BETWEEN t_start AND t_end;
    -- Also, invalidate the histograms where this JobID used to be before.
    UPDATE t_c_category_data SET t_outdated=NOW() WHERE x_old_t BETWEEN t_start AND t_end;

    -- Insert empty WN histograms if it doesn't exist yet
    SET x_t_start = STR_TO_DATE(DATE_FORMAT(latest_t_site, "%Y-%m-%d 00:00:00"),'%Y-%m-%d %H:%i:%s');
    SET x_t_end   = STR_TO_DATE(DATE_FORMAT(latest_t_site, "%Y-%m-%d 23:59:59"),'%Y-%m-%d %H:%i:%s');
    INSERT IGNORE INTO t_c_category_data_wns (interval_type, wn, t_start, t_end, t_outdated) VALUES ('day', latest_worker_node_site, x_t_start, x_t_end, NOW());

    SET x_t_start = STR_TO_DATE(DATE_FORMAT(latest_t_site, "%Y-%m-%d %H:00:00"),'%Y-%m-%d %H:%i:%s');
    SET x_t_end   = STR_TO_DATE(DATE_FORMAT(latest_t_site, "%Y-%m-%d %H:59:59"),'%Y-%m-%d %H:%i:%s');
    INSERT IGNORE INTO t_c_category_data_wns (interval_type, wn, t_start, t_end, t_outdated) VALUES ('hour', latest_worker_node_site, x_t_start, x_t_end, NOW());

    -- Invalidate WN histograms
    UPDATE t_c_category_data_wns SET t_outdated=NOW() WHERE wn = latest_worker_node_site AND latest_t_site BETWEEN t_start AND t_end;
    UPDATE t_c_category_data_wns SET t_outdated=NOW() WHERE wn = x_old_wn AND x_old_t BETWEEN t_start AND t_end;
  END IF;

END; #PROCEDURE
//
DELIMITER ;


DROP PROCEDURE IF EXISTS p_update_histogram;
DELIMITER //
CREATE PROCEDURE p_update_histogram
(
  IN in_t DATETIME,
  IN in_wn VARCHAR(255),
  IN in_interval_type VARCHAR(32)
)
MODIFIES SQL DATA
this_procedure:BEGIN
  # PROCEDURE p_update_histogram
  #
  # Creates or updates the cached histogram in t_c_category_data of the interval of type interval_type and the time given in in_t.
  # Supported values for interval_type: 'day', 'hour'
  # If in_wn is specified (not NULL), the histogram will be written to t_c_category_data_wns.
  # Otherwise, it will be written to t_c_category_data.

  DECLARE x_t_start DATETIME;
  DECLARE x_t_end DATETIME;

  DECLARE x_cat0 INT DEFAULT 0;
  DECLARE x_cat1 INT DEFAULT 0;
  DECLARE x_cat2 INT DEFAULT 0;
  DECLARE x_cat4 INT DEFAULT 0;
  DECLARE x_cat3 INT DEFAULT 0;
  DECLARE x_cat5 INT DEFAULT 0;
  DECLARE x_cat6 INT DEFAULT 0;
  DECLARE x_cat7 INT DEFAULT 0;
  DECLARE x_cat8 INT DEFAULT 0;
  DECLARE x_cat9 INT DEFAULT 0;
  DECLARE x_cat10 INT DEFAULT 0;
  DECLARE x_cat11 INT DEFAULT 0;
  DECLARE x_cat12 INT DEFAULT 0;
  DECLARE x_cat13 INT DEFAULT 0;
  DECLARE x_cat14 INT DEFAULT 0;
  DECLARE x_cat15 INT DEFAULT 0;
  DECLARE x_total INT DEFAULT 0;
  DECLARE x_cat6_1 INT DEFAULT 0;

  # Walltimes
  DECLARE x_t_ds BIGINT DEFAULT 0;
  DECLARE x_t_fs BIGINT DEFAULT 0;
  DECLARE x_t_do BIGINT DEFAULT 0;
  DECLARE x_t_fo BIGINT DEFAULT 0;

  # KPIs
  DECLARE x_kpi_k1 DECIMAL(18,2) DEFAULT NULL;
  DECLARE x_kpi_k2 DECIMAL(18,2) DEFAULT NULL;
  DECLARE x_kpi_s2 DECIMAL(18,2) DEFAULT NULL;
  DECLARE x_kpi_t1 DECIMAL(18,2) DEFAULT NULL;
  DECLARE x_kpi_t1_1 DECIMAL(18,2) DEFAULT 0; # Like T1, but without those cat6 jobs where the JobID has its latest state begin with "Killed: using more than"

  # Kilian-KPIs
  DECLARE x_kpi_k1_total_ds INT DEFAULT 0;
  DECLARE x_kpi_k1_total_ds_ohne_8 INT DEFAULT 0;
  DECLARE x_kpi_k1_total_fs INT DEFAULT 0;
  DECLARE x_kpi_k1_total_do INT DEFAULT 0;
  DECLARE x_kpi_k1_total_fo INT DEFAULT 0;

  # Thorsten-KPIs
  DECLARE x_total_jobIDs INT DEFAULT 0;

  IF in_interval_type = 'day' THEN
    SET x_t_start = STR_TO_DATE(DATE_FORMAT(in_t, "%Y-%m-%d 00:00:00"),'%Y-%m-%d %H:%i:%s');
    SET x_t_end   = STR_TO_DATE(DATE_FORMAT(in_t, "%Y-%m-%d 23:59:59"),'%Y-%m-%d %H:%i:%s');
  ELSEIF in_interval_type = 'hour' THEN
    SET x_t_start = STR_TO_DATE(DATE_FORMAT(in_t, "%Y-%m-%d %H:00:00"),'%Y-%m-%d %H:%i:%s');
    SET x_t_end   = STR_TO_DATE(DATE_FORMAT(in_t, "%Y-%m-%d %H:59:59"),'%Y-%m-%d %H:%i:%s');
  ELSE
    LEAVE this_procedure;
  END IF;

  IF x_t_start IS NULL THEN
    LEAVE this_procedure;
  END IF;

  DROP TEMPORARY TABLE IF EXISTS tmp_categories;
  CREATE TEMPORARY TABLE tmp_categories (category INT, number_absolute INT);

  IF in_wn IS NULL THEN
    START TRANSACTION;
      INSERT INTO tmp_categories (category, number_absolute)
        SELECT
          j.c_category AS category,
          COUNT(j.job_id) AS number_absolute
        FROM t_jobs AS j
        LEFT JOIN t_config_categories AS c ON c.category_id=j.c_category
        WHERE NOT j.c_latest_t_site<x_t_start AND NOT j.c_latest_t_site>x_t_end
        GROUP BY j.c_category;

      SELECT COUNT(*)
      FROM t_jobs
      WHERE NOT c_latest_t_site<x_t_start AND NOT c_latest_t_site>x_t_end
      AND c_category=6
      AND c_latest_state_site NOT LIKE "Killed: using more than%"
      INTO x_cat6_1; #does NOT exclude those JobIDs where the "Killed..." state happened in the non-latest state.

      SELECT
        SUM(c_t_ds),
        SUM(c_t_fs),
        SUM(c_t_do),
        SUM(c_t_fo)
      FROM t_jobs AS j
      WHERE NOT j.c_latest_t_site<x_t_start AND NOT j.c_latest_t_site>x_t_end
      INTO
        x_t_ds,
        x_t_fs,
        x_t_do,
        x_t_fo;
    COMMIT;
  ELSE
    START TRANSACTION;
      INSERT INTO tmp_categories (category, number_absolute)
        SELECT
          j.c_category AS category,
          COUNT(j.job_id) AS number_absolute
        FROM t_jobs AS j
        LEFT JOIN t_config_categories AS c ON c.category_id=j.c_category
        WHERE NOT j.c_latest_t_site<x_t_start AND NOT j.c_latest_t_site>x_t_end
        AND j.c_latest_worker_node_site=in_wn
        GROUP BY j.c_category;

      SELECT
        SUM(c_t_ds),
        SUM(c_t_fs),
        SUM(c_t_do),
        SUM(c_t_fo)
      FROM t_jobs AS j
      WHERE NOT j.c_latest_t_site<x_t_start AND NOT j.c_latest_t_site>x_t_end
      AND j.c_latest_worker_node_site=in_wn
      INTO
        x_t_ds,
        x_t_fs,
        x_t_do,
        x_t_fo;
    COMMIT;
  END IF;

  SET x_cat0  = (SELECT number_absolute FROM tmp_categories WHERE category = 0);
  SET x_cat1  = (SELECT number_absolute FROM tmp_categories WHERE category = 1);
  SET x_cat2  = (SELECT number_absolute FROM tmp_categories WHERE category = 2);
  SET x_cat3  = (SELECT number_absolute FROM tmp_categories WHERE category = 3);
  SET x_cat4  = (SELECT number_absolute FROM tmp_categories WHERE category = 4);
  SET x_cat5  = (SELECT number_absolute FROM tmp_categories WHERE category = 5);
  SET x_cat6  = (SELECT number_absolute FROM tmp_categories WHERE category = 6);
  SET x_cat7  = (SELECT number_absolute FROM tmp_categories WHERE category = 7);
  SET x_cat8  = (SELECT number_absolute FROM tmp_categories WHERE category = 8);
  SET x_cat9  = (SELECT number_absolute FROM tmp_categories WHERE category = 9);
  SET x_cat10 = (SELECT number_absolute FROM tmp_categories WHERE category = 10);
  SET x_cat11 = (SELECT number_absolute FROM tmp_categories WHERE category = 11);
  SET x_cat12 = (SELECT number_absolute FROM tmp_categories WHERE category = 12);
  SET x_cat13 = (SELECT number_absolute FROM tmp_categories WHERE category = 13);
  SET x_cat14 = (SELECT number_absolute FROM tmp_categories WHERE category = 14);
  SET x_cat15 = (SELECT number_absolute FROM tmp_categories WHERE category = 15);

  IF x_cat0  IS NULL THEN SET x_cat0  = 0; END IF;
  IF x_cat1  IS NULL THEN SET x_cat1  = 0; END IF;
  IF x_cat2  IS NULL THEN SET x_cat2  = 0; END IF;
  IF x_cat3  IS NULL THEN SET x_cat3  = 0; END IF;
  IF x_cat4  IS NULL THEN SET x_cat4  = 0; END IF;
  IF x_cat5  IS NULL THEN SET x_cat5  = 0; END IF;
  IF x_cat6  IS NULL THEN SET x_cat6  = 0; END IF;
  IF x_cat7  IS NULL THEN SET x_cat7  = 0; END IF;
  IF x_cat8  IS NULL THEN SET x_cat8  = 0; END IF;
  IF x_cat9  IS NULL THEN SET x_cat9  = 0; END IF;
  IF x_cat10 IS NULL THEN SET x_cat10 = 0; END IF;
  IF x_cat11 IS NULL THEN SET x_cat11 = 0; END IF;
  IF x_cat12 IS NULL THEN SET x_cat12 = 0; END IF;
  IF x_cat13 IS NULL THEN SET x_cat13 = 0; END IF;
  IF x_cat14 IS NULL THEN SET x_cat14 = 0; END IF;
  IF x_cat15 IS NULL THEN SET x_cat15 = 0; END IF;
  SET x_total = x_cat0 + x_cat1 + x_cat2 + x_cat3 + x_cat4 + x_cat5 + x_cat6 + x_cat7 + x_cat8 + x_cat9 + x_cat10 + x_cat11 + x_cat12 + x_cat13 + x_cat14 + x_cat15;

  IF x_t_ds IS NULL THEN SET x_t_ds = 0; END IF;
  IF x_t_fs IS NULL THEN SET x_t_fs = 0; END IF;
  IF x_t_do IS NULL THEN SET x_t_do = 0; END IF;
  IF x_t_fo IS NULL THEN SET x_t_fo = 0; END IF;

  # Kilian-KPIs
  SET x_kpi_k1_total_ds=(SELECT SUM(number_absolute) FROM tmp_categories WHERE category IN (8,9,10,11,12,13,14,15));
  SET x_kpi_k1_total_ds_ohne_8=(SELECT SUM(number_absolute) FROM tmp_categories WHERE category IN (9,10,11,12,13,14,15));
  SET x_kpi_k1_total_fs=(SELECT SUM(number_absolute) FROM tmp_categories WHERE category IN (4,5,6,7,12,13,14,15));
  SET x_kpi_k1_total_do=(SELECT SUM(number_absolute) FROM tmp_categories WHERE category IN (2,3,6,7,10,11,14,15));
  SET x_kpi_k1_total_fo=(SELECT SUM(number_absolute) FROM tmp_categories WHERE category IN (1,3,5,7,9,11,13,15));
  IF x_kpi_k1_total_fs*x_kpi_k1_total_do>0 THEN
    SET x_kpi_k1 = 100.*x_kpi_k1_total_ds*x_kpi_k1_total_fo/(x_kpi_k1_total_fs*x_kpi_k1_total_do);
    SET x_kpi_k2 = 100.*x_kpi_k1_total_ds_ohne_8*x_kpi_k1_total_fo/(x_kpi_k1_total_fs*x_kpi_k1_total_do);
  ELSE
    SET x_kpi_k1 = NULL;
    SET x_kpi_k2 = NULL;
  END IF;

  # Sören-KPIs
  IF x_cat6>0 THEN
    SET x_kpi_s2 = 100.*x_cat9/x_cat6;
  ELSE
    SET x_kpi_s2 = NULL;
  END IF;

  # Thorsten-KPIs
  SET x_total_jobIDs = (SELECT SUM(number_absolute) FROM tmp_categories);
  IF x_total_jobIDs>0 THEN
    SET x_kpi_t1 = 100.*x_cat6/x_total_jobIDs;
    SET x_kpi_t1_1 = 100.*x_cat6_1/x_total_jobIDs;
  ELSE
    SET x_kpi_t1 = NULL;
    SET x_kpi_t1_1 = NULL;
  END IF;

  IF in_wn IS NULL THEN
    START TRANSACTION;
      INSERT INTO t_c_category_data(
        interval_type,
        t_start,
        t_end,
        cat0,
        cat1,
        cat2,
        cat3,
        cat4,
        cat5,
        cat6,
        cat7,
        cat8,
        cat9,
        cat10,
        cat11,
        cat12,
        cat13,
        cat14,
        cat15,
        total,
        t_ds,
        t_fs,
        t_do,
        t_fo,
        kpi_k1,
        kpi_k2,
        kpi_s2,
        kpi_t1,
        kpi_t1_1,
        t_updated,
        t_outdated
        )
        VALUES(
        in_interval_type,
        x_t_start,
        x_t_end,
        x_cat0,
        x_cat1,
        x_cat2,
        x_cat3,
        x_cat4,
        x_cat5,
        x_cat6,
        x_cat7,
        x_cat8,
        x_cat9,
        x_cat10,
        x_cat11,
        x_cat12,
        x_cat13,
        x_cat14,
        x_cat15,
        x_total,
        x_t_ds,
        x_t_fs,
        x_t_do,
        x_t_fo,
        x_kpi_k1,
        x_kpi_k2,
        x_kpi_s2,
        x_kpi_t1,
        x_kpi_t1_1,
        NOW(),
        NULL
        )
        ON DUPLICATE KEY UPDATE
        cat0  = x_cat0,
        cat1  = x_cat1,
        cat2  = x_cat2,
        cat3  = x_cat3,
        cat4  = x_cat4,
        cat5  = x_cat5,
        cat6  = x_cat6,
        cat7  = x_cat7,
        cat8  = x_cat8,
        cat9  = x_cat9,
        cat10 = x_cat10,
        cat11 = x_cat11,
        cat12 = x_cat12,
        cat13 = x_cat13,
        cat14 = x_cat14,
        cat15 = x_cat15,
        total = x_total,
        t_ds = x_t_ds,
        t_fs = x_t_fs,
        t_do = x_t_do,
        t_fo = x_t_fo,
        kpi_k1 = x_kpi_k1,
        kpi_k2 = x_kpi_k2,
        kpi_s2 = x_kpi_s2,
        kpi_t1 = x_kpi_t1,
        kpi_t1_1 = x_kpi_t1_1,
        t_updated = NOW(),
        t_outdated = NULL;
    COMMIT;
  ELSE
    START TRANSACTION;
      INSERT INTO t_c_category_data_wns(
        interval_type,
        wn,
        t_start,
        t_end,
        cat0,
        cat1,
        cat2,
        cat3,
        cat4,
        cat5,
        cat6,
        cat7,
        cat8,
        cat9,
        cat10,
        cat11,
        cat12,
        cat13,
        cat14,
        cat15,
        total,
        t_ds,
        t_fs,
        t_do,
        t_fo,
        kpi_k1,
        kpi_k2,
        kpi_s2,
        kpi_t1,
        t_updated,
        t_outdated
        )
        VALUES(
        in_interval_type,
        in_wn,
        x_t_start,
        x_t_end,
        x_cat0,
        x_cat1,
        x_cat2,
        x_cat3,
        x_cat4,
        x_cat5,
        x_cat6,
        x_cat7,
        x_cat8,
        x_cat9,
        x_cat10,
        x_cat11,
        x_cat12,
        x_cat13,
        x_cat14,
        x_cat15,
        x_total,
        x_t_ds,
        x_t_fs,
        x_t_do,
        x_t_fo,
        x_kpi_k1,
        x_kpi_k2,
        x_kpi_s2,
        x_kpi_t1,
        NOW(),
        NULL
        )
        ON DUPLICATE KEY UPDATE
        cat0  = x_cat0,
        cat1  = x_cat1,
        cat2  = x_cat2,
        cat3  = x_cat3,
        cat4  = x_cat4,
        cat5  = x_cat5,
        cat6  = x_cat6,
        cat7  = x_cat7,
        cat8  = x_cat8,
        cat9  = x_cat9,
        cat10 = x_cat10,
        cat11 = x_cat11,
        cat12 = x_cat12,
        cat13 = x_cat13,
        cat14 = x_cat14,
        cat15 = x_cat15,
        total = x_total,
        t_ds = x_t_ds,
        t_fs = x_t_fs,
        t_do = x_t_do,
        t_fo = x_t_fo,
        kpi_k1 = x_kpi_k1,
        kpi_k2 = x_kpi_k2,
        kpi_s2 = x_kpi_s2,
        kpi_t1 = x_kpi_t1,
        t_updated = NOW(),
        t_outdated = NULL;
    COMMIT;
  END IF;
END; #PROCEDURE
//
DELIMITER ;


DROP PROCEDURE IF EXISTS p_results_custom_histogram;
DELIMITER //
CREATE PROCEDURE p_results_custom_histogram
(
  in_t1 DATETIME,
  in_t2 DATETIME
)
READS SQL DATA
BEGIN
  # PROCEDURE p_results_custom_histogram
  #

  DECLARE num_total INT;
  DECLARE num_cat0 INT;
  DECLARE num_cat1 INT;
  DECLARE num_cat2 INT;
  DECLARE num_cat3 INT;
  DECLARE num_cat4 INT;
  DECLARE num_cat5 INT;
  DECLARE num_cat6 INT;
  DECLARE num_cat7 INT;
  DECLARE num_cat8 INT;
  DECLARE num_cat9 INT;
  DECLARE num_cat10 INT;
  DECLARE num_cat11 INT;
  DECLARE num_cat12 INT;
  DECLARE num_cat13 INT;
  DECLARE num_cat14 INT;
  DECLARE num_cat15 INT;
  SET num_total = (SELECT SUM(total) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat0 = (SELECT SUM(cat0) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat1 = (SELECT SUM(cat1) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat2 = (SELECT SUM(cat2) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat3 = (SELECT SUM(cat3) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat4 = (SELECT SUM(cat4) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat5 = (SELECT SUM(cat5) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat6 = (SELECT SUM(cat6) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat7 = (SELECT SUM(cat7) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat8 = (SELECT SUM(cat8) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat9 = (SELECT SUM(cat9) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat10 = (SELECT SUM(cat10) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat11 = (SELECT SUM(cat11) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat12 = (SELECT SUM(cat12) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat13 = (SELECT SUM(cat13) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat14 = (SELECT SUM(cat14) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);
  SET num_cat15 = (SELECT SUM(cat15) FROM t_c_category_data WHERE interval_type='hour' AND NOT t_end<in_t1 AND NOT t_start>in_t2);

  SELECT "00" AS sort, "cat 0" AS description, num_cat0 AS num_abs, 100.*num_cat0/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=0) AS longdescription
  UNION
  SELECT "01" AS sort, "cat 1" AS description, num_cat1 AS num_abs, 100.*num_cat1/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=1) AS longdescription
  UNION
  SELECT "02" AS sort, "cat 2" AS description, num_cat2 AS num_abs, 100.*num_cat2/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=2) AS longdescription
  UNION
  SELECT "03" AS sort, "cat 3" AS description, num_cat3 AS num_abs, 100.*num_cat3/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=3) AS longdescription
  UNION
  SELECT "04" AS sort, "cat 4" AS description, num_cat4 AS num_abs, 100.*num_cat4/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=4) AS longdescription
  UNION
  SELECT "05" AS sort, "cat 5" AS description, num_cat5 AS num_abs, 100.*num_cat5/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=5) AS longdescription
  UNION
  SELECT "06" AS sort, "cat 6" AS description, num_cat6 AS num_abs, 100.*num_cat6/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=6) AS longdescription
  UNION
  SELECT "07" AS sort, "cat 7" AS description, num_cat7 AS num_abs, 100.*num_cat7/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=7) AS longdescription
  UNION
  SELECT "08" AS sort, "cat 8" AS description, num_cat8 AS num_abs, 100.*num_cat8/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=8) AS longdescription
  UNION
  SELECT "09" AS sort, "cat 9" AS description, num_cat9 AS num_abs, 100.*num_cat9/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=9) AS longdescription
  UNION
  SELECT "10" AS sort, "cat 10" AS description, num_cat10 AS num_abs, 100.*num_cat10/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=10) AS longdescription
  UNION
  SELECT "11" AS sort, "cat 11" AS description, num_cat11 AS num_abs, 100.*num_cat11/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=11) AS longdescription
  UNION
  SELECT "12" AS sort, "cat 12" AS description, num_cat12 AS num_abs, 100.*num_cat12/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=12) AS longdescription
  UNION
  SELECT "13" AS sort, "cat 13" AS description, num_cat13 AS num_abs, 100.*num_cat13/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=13) AS longdescription
  UNION
  SELECT "14" AS sort, "cat 14" AS description, num_cat14 AS num_abs, 100.*num_cat14/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=14) AS longdescription
  UNION
  SELECT "15" AS sort, "cat 15" AS description, num_cat15 AS num_abs, 100.*num_cat15/num_total AS num_pct, (SELECT description FROM t_config_categories WHERE category_id=15) AS longdescription;

END; #PROCEDURE
//
DELIMITER ;
