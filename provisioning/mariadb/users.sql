CREATE USER IF NOT EXISTS 'alien'@'localhost' IDENTIFIED BY 'EsR4SDiM1n';
GRANT ALL ON alien.* TO 'alien'@'localhost';

CREATE USER IF NOT EXISTS 'alienfrontend'@'localhost' IDENTIFIED BY 'gga97HsjD55G4YaQkgqL';
GRANT SELECT ON alien.* TO 'alienfrontend'@'localhost';
GRANT EXECUTE ON FUNCTION alien.f_results_kpi_t1 TO 'alienfrontend'@'localhost';
GRANT EXECUTE ON FUNCTION alien.f_results_kpi_s2 TO 'alienfrontend'@'localhost';
