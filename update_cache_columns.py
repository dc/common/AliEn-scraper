#!/usr/bin/python2
# -*- coding: utf-8 -*-

import time         #sleep, parse timestamp
import datetime
import subprocess   #call external processes
import sys          #command line arguments
import pprint       #pretty printing of primitive objects
import MySQLdb as mdb
import re           #regular expressions
import threading    #threads
from threading import Lock
import logging      #
from collections import deque   # deque
import os           # clear screen
import datetime
import signal       # Ctrl+C

# Database credentials
db_host = "localhost"
db_user = "alien"
db_pass = "EsR4SDiM1n"
db_db   = "alien"


def database_connection():
    return mdb.connect(db_host, db_user, db_pass, db_db)

# Logging
FORMAT = '%(asctime)s (%(threadName)-10s) %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format=FORMAT)


logging.info("Selecting list of all job_ids in t_jobs.")

dbc = database_connection()

cur = dbc.cursor()
cur.execute("SELECT job_id FROM v_queue_ps WHERE scrape_reason>2000 ORDER BY job_id ASC")
job_ids = cur.fetchall()
cur.close()

logging.info("%d rows are to be updated." % len(job_ids))

N=0

t1 = datetime.datetime.now()

for i_job_id in job_ids:
    cur = dbc.cursor()
    cur.callproc("p_update_job_details", [ i_job_id ])
    proc_result = cur.fetchone()
    cur.close()
    N+=1

    if N%1000==0: # Log a message only show every 1000 lines
        t2 = datetime.datetime.now()
        remaining_job_ids = N - len(job_ids)
        speed = 1000 / (t2-t1).seconds
        eta = -remaining_job_ids / speed
        t1 = t2

        logging.debug("Processed %d of %d rows. ETA = %s" % (N, len(job_ids), str(datetime.timedelta(seconds=eta))))
        dbc.commit()

logging.debug("Processed %d of %d rows" % (N, len(job_ids)))

dbc.commit()

logging.debug("Done.")
